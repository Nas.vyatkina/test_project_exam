import requests
import time
from termcolor import colored
import json
import datetime


class baseWorkWithNetwork:

    # метод POST
    def test_post_method(self,url_method, json_entry, headers):
        start_time = time.time()
        result = requests.post(url=url_method, json=json_entry, headers=headers)
        end_time = time.time()
        time_work = end_time - start_time
        return result.json(), time_work, result.headers, result.status_code

    # метод GET
    def test_get_method(self, url_method, headers, params):
        start_time = time.time()
        result = requests.get(url=url_method, headers=headers, params=params)
        end_time = time.time()
        time_work = end_time - start_time
        return result.json(), time_work, result.headers, result.status_code

    # метод HEAD
    def test_head_method(self, url_method, headers, params):
        start_time = time.time()
        result = requests.head(url=url_method, headers=headers, params=params)
        end_time = time.time()
        time_work = end_time - start_time
        return time_work, result.headers, result.status_code

    # метод PUT
    def test_put_method(self,url_method, json_entry, headers):
        start_time = time.time()
        result = requests.put(url=url_method, json=json_entry, headers=headers)
        end_time = time.time()
        time_work = end_time - start_time
        return time_work, result.headers, result.status_code

    # метод DELETE
    def test_delete_method(self, url_method, params, headers):
        start_time = time.time()
        result = requests.delete(url=url_method, params=params, headers=headers)
        end_time = time.time()
        time_work = end_time - start_time
        return result.json(), time_work, result.headers, result.status_code


class baseMicrotest(baseWorkWithNetwork):
    """
        type    string  POST, GET,
    """
    microtest = {
        "index": None,
        "name": None,
        "status": True,
        "message": None,
        "time_work": None}

    return_headers = None
    status_code = None
    f = "\t\t\t"
    __message = "Входные параметры:\n {f}{jE} \n{f}{p} \n{f} Headers:\n {f}{h} \n\n{f} Выходные параметры:\n " \
                "{f}Status code = {SC} \n {f}{rM}{mesResOut}\n"

    def req_func(self, dict_, result_json, keys):
        res = []
        for key in keys:
            if type(key) == str:
                if dict_.get(key, False):
                    if (type(result_json.get(key, False)) != dict_[key]["type"]) is True:
                        res.append(key)
                    else:
                        if dict_[key]["next"] is not None:
                            if type(result_json[key]) == list:
                                key_next = range(len(result_json[key]))
                            else:
                                key_next = result_json[key].keys()
                            tmp = self.req_func(dict_=dict_[key]["next"], result_json=result_json[key], keys=key_next)
                            if len(tmp) > 0:
                                res += tmp
                else:
                    res.append(key)
            else:
                try:
                    if ((type(result_json[key])) != dict_[0]["type"]) is True:
                        res.append(key)
                    else:
                        if dict_[0]["next"] is not None:
                            if type(result_json[key]) == list:
                                key_next = range(len(result_json[key]))
                            else:
                                key_next = result_json[key].keys()
                            tmp = self.req_func(dict_=dict_[0]["next"], result_json=result_json[key], keys=key_next)
                            if len(tmp) > 0:
                                res += tmp
                except IndexError:
                    res.append(key)
        return res

    def __init__(self, headers, type, url, response_code, check_for_output, struct_microtest,
                 json_entry=False, params=False):
        assert type in ["post", "get", "put", "delete", "head"]
        self.__clean()
        if headers.get("Token", False):
            try:
                headers["Token"] = headers["Token"]()
            except TypeError:
                pass
            except Exception as e:
                print(e)

        self.headers = headers

        self.microtest["index"] = struct_microtest["index"]
        self.microtest["name"] = struct_microtest["name"]
        self.type = type
        self.url = url
        if not json_entry:
            self.json_entry = ""
        else:
            self.json_entry = json_entry
        if not params:
            self.params = ""
        else:
            self.params = params
        self.__response_code = response_code
        self.check_for_output = check_for_output
        self.message_check_output = ""

    def __clean(self):
        self.microtest = {
            "index": None,
            "name": None,
            "status": True,
            "message": None,
            "time_work": None}
        self.return_headers = None

    def __action(self, action, result, message, status_code, mes_res_output):
        if action is None:
            pass
        elif action is True:
            self.microtest["message"] = message.format(
                jE=self.json_entry,
                p=self.params,
                h=self.headers,
                f=self.f,
                SC=status_code,
                rM=result,
                mesResOut=mes_res_output)
        elif action is False:
            self.microtest["message"] = message.format(
                jE=self.json_entry,
                p=self.params,
                f=self.f,
                h=self.headers,
                SC=status_code,
                rM=result,
                mesResOut=mes_res_output)
            self.microtest["status"] = False

    def start(self):
        #print(self.type)
        result = {}
        if self.type == "post":
            result, self.microtest["time_work"], self.return_headers, self.status_code = self.test_post_method(
                                                                                            url_method=self.url,
                                                                                            json_entry=self.json_entry,
                                                                                            headers=self.headers)
        elif self.type == "get":
            result, self.microtest["time_work"], self.return_headers, self.status_code = self.test_get_method(
                                                                                                url_method=self.url,
                                                                                                headers=self.headers,
                                                                                                params=self.params)
        elif self.type == "put":
            self.microtest["time_work"], self.return_headers, self.status_code = self.test_put_method(
                                                                                        url_method=self.url,
                                                                                        json_entry=self.json_entry,
                                                                                        headers=self.headers)
        elif self.type == "delete":
            result, self.microtest["time_work"], self.return_headers, self.status_code = self.test_delete_method(
                                                                                                url_method=self.url,
                                                                                                headers=self.headers,
                                                                                                params=self.params)
        elif self.type == "head":
            self.microtest["time_work"], self.return_headers, self.status_code = self.test_head_method(
                                                                                        url_method=self.url,
                                                                                        headers=self.headers,
                                                                                        params=self.params)

        # если есть структура для проверки получаемого json, то проверяем

        if self.check_for_output:
            keys = result.keys()
            self.message_check_output = self.req_func(dict_=self.check_for_output, result_json=result, keys=keys)

        resp_code = str(self.status_code)
        #print(resp_code)
        if self.message_check_output:
            self.message_check_output = "\n\n\t\t\tНеверный тип получаемого ключа/ключ отсутствует " + \
                                        str(self.message_check_output)
            self.__action(action=False,
                          result=result,
                          message=self.__message,
                          status_code=resp_code,
                          mes_res_output=self.message_check_output)

        else:
            if self.__response_code.get(resp_code, False):
                action = self.__response_code.get(resp_code)
                self.__action(action=action["action"],
                              result=result,
                              message=self.__message,
                              status_code=resp_code,
                              mes_res_output="")

            else:  # в том случае если код ответа не найден в нашей входной структуре
                #action = self.__response_code.get("else")
                self.__action(action=False,
                              result=result,
                              message="Ответ не соответствует документации.\n\t\t\t" + self.__message,
                              status_code=resp_code,
                              mes_res_output="")


class baseTest():
    def __init__(self, name, type_, microtest_data, name_file, DIR, num):
        self.input = None
        assert type(name) == str
        self.name = name
        self.status = True
        self.microtest_data = microtest_data
        self.data = []
        self.name_file = name_file
        self.type = type_
        self.DIR = DIR
        self.num = num

    def __print_format(self):
        if self.status:
            status_all = colored(self.status, "green")
        else:
            status_all = colored(self.status, "red")

        print("test {number}. {name}. Status {status} \n".format(
            number=self.num,
            name=self.name,
            status=status_all))

        for microtest in self.data:
            if microtest["status"]:
                microtest_status = colored(microtest["status"], "green")
            else:
                microtest_status = colored(microtest["status"], "red")

            print("\t\t {index}. {name}. Status {status}. Time {time:.3f}s \n".format(
                index=microtest["index"],
                name=microtest["name"],
                status=microtest_status,
                time=microtest["time_work"]))
            if microtest["message"]:

                print("\t\t\t {message} \n".format(message=microtest["message"]))

    def __save_result_html(self):
        my_file = open(self.name_file + ".html", "a", encoding="utf-16")
        if self.status:
            status_all_html = "<font style='color:#00ff00'> {s} </font>".format(s=self.status)
        else:
            status_all_html = "<font style='color:#ff0000'> {s} </font>".format(s=self.status)
        test_string = ("test {number}. {name}. Status {status} </br>".format(
                        number=self.num,
                        name=self.name,
                        status=status_all_html))
        my_file.write(test_string)

        for microtest in self.data:
            if microtest["status"]:
                microtest_string_html = "<font style='color:#00ff00'> {s} </font>".format(s=microtest["status"])
            else:
                microtest_string_html = "<font style='color:#ff0000'> {s} </font>".format(s=microtest["status"])
            microtest_string = ("<span style='padding: 0px 40px;'>{index}. {name}. Status {status}. Time {time:.3f}s</span></br>".format(
                                index=microtest["index"],
                                name=microtest["name"],
                                status=microtest_string_html,
                                time=microtest["time_work"]))
            my_file.write(microtest_string)
            if microtest["message"]:

                mes = microtest["message"].replace("\n", "</br>")
                mes = mes.replace("\t\t\t", "<span style='padding: 0px 40px;'></span>")
                mes_string = ("<span style='padding: 0px 80px;'>{message} </span>".format(message=mes))
                my_file.write(mes_string)
            my_file.write("</br>")

        my_file.close()

    def __save_result_txt(self):
        my_file = open(self.name_file + ".txt", "a", encoding="utf-16")

        test_string = ("test {number}. {name}. Status {status} \n".format(
                        number=self.num,
                        name=self.name,
                        status=self.status))
        my_file.write(test_string)

        for microtest in self.data:

            microtest_string = ("\t\t{index}. {name}. Status {status}. Time {time:.3f}s".format(
                                index=microtest["index"],
                                name=microtest["name"],
                                status=microtest["status"],
                                time=microtest["time_work"]))
            my_file.write(microtest_string)
            if microtest["message"]:
                my_file.write(microtest["message"])
            my_file.write("\n")

        my_file.close()

    def start(self):
        start = 65  # если количсество микротестов больше 26 - придумать как добавлять АААААА
        index, input = 0, self.microtest_data
        #print(input)
        input["struct_microtest"]["index"] = chr(start + index)
        microtest = baseMicrotest(
            headers=input["headers"],
            struct_microtest=input["struct_microtest"],
            type=self.type,
            url=input["url"],
            json_entry=input["body"],
            params=input["query"],
            response_code=input["responses"],
            check_for_output=input["dict_check_for_output"]
            )
        microtest.start()
        if microtest.microtest["status"] is False:
            self.status = False
        self.data.append(microtest.microtest)


        with open(self.DIR + '_data_unsuccessful.json') as data_file:
            data_ = json.load(data_file)
        #pprint(data)
        for row in range(len(data_["data"])):
            index, input = row+1, data_["data"][row]
            input["struct_microtest"]["index"] = chr(start + index)
            microtest = baseMicrotest(
                headers=input["headers"],
                type=self.type,
                struct_microtest=input["struct_microtest"],
                url=data_["url"],
                json_entry=input["body"],
                params=input["query"],
                response_code=data_["responses"],
                check_for_output=data_["dict_check_for_output"]
            )

            microtest.start()
            if microtest.microtest["status"] is False:
                self.status = False
            self.data.append(microtest.microtest)

        self.__print_format()
        self.__save_result_html()
        self.__save_result_txt()