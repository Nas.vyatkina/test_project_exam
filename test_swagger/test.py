import json
import os
from pprint import pprint
from exam_unit_test_class import baseTest

def replacement(data, keys):
    check_type = {
        "list": list,
        "string": str,
        "dict": dict,
        "integer": int,
        "boolean": bool,
        "number": float
    }

    for key in keys:
        type_now = data[key]['type']
        data[key]['type'] = check_type[type_now]
        if data[key]['next'] is not None:
            if type(data[key]["next"]) == list:
                keys_next = range(len(data[key]["next"]))
            else:
                keys_next = data[key]["next"].keys()
            replacement(data[key]["next"], keys_next)
    return data


if __name__ == "__main__":
    PATH_TO_DIR = os.getcwd()
    with open('directories.json') as data_file:
        data_dir = json.load(data_file)
    section = []
    index = 0
    keys = []
    methods = []
    keys_methods = []
    key_section = 0
    for i in data_dir.keys():
        keys.append(i)
        section.append(str(index) + " - " + str(i))
        index += 1
    quest = '''Выберите раздел, для которого нужно провести тест:\n{} \n+ - для всех разделов'''.format(
                                                                                        '\n'.join(section))

    print(quest)
    tag = input()
    if tag != "+":
        key_section = keys[int(tag)]
        print(key_section)
        #print(data[key])
        index = 0
        for i in range(len(data_dir[key_section])):
            keys_methods.append(data_dir[key_section][i])
            methods.append(str(index) + " - " + str(data_dir[key_section][i]))
            index += 1
        quest = '''Выберите метод, для которого нужно провести тест:\n{} \n+ - для всех методов'''.format(
                                                                                        '\n'.join(methods))
        print(quest)
        method_num = input()
        if method_num != "+":
            key_method = keys_methods[int(method_num)]

    name_file = "Result_test"
    open(name_file + ".html", "w").close()
    open(name_file + ".txt", "w").close()
    if tag == "+":
        range_for = keys
        num_test = 1
        for test in range_for:

            for method in data_dir[test]:
                DIR = PATH_TO_DIR + '/tests/' + str(test) + "/" + str(method)
                with open(DIR + '_data_successful.json') as data_file:
                    data = json.load(data_file)

                dict_check_dump = data["dict_check_for_output"]
                if dict_check_dump:
                    replacement(data=dict_check_dump, keys=dict_check_dump.keys())
                #pprint(data)
                test_ = baseTest(
                    name=data["summary"],
                    num=num_test,
                    microtest_data=data,
                    name_file=name_file,
                    type_=data["method"],
                    DIR=DIR)
                mes_test = test_.start()
                num_test += 1

    else:
        range_for = int(tag)
        #name_file_key = keys[int(tag)]

        num_test = 1
        if method_num == "+":
            for row in keys_methods:
                DIR = PATH_TO_DIR + '/tests/' + str(key_section) + "/" + str(row)
                with open( DIR + '_data_successful.json') as data_file:
                    data = json.load(data_file)
                #print(row)

                dict_check_dump = data["dict_check_for_output"]
                if dict_check_dump:
                    replacement(data=dict_check_dump, keys=dict_check_dump.keys())

                test = baseTest(
                    name=data["summary"],
                    num=num_test,
                    microtest_data=data,
                    name_file=name_file,
                    type_=data["method"],
                    DIR=DIR
                )
                mes_test = test.start()
                num_test += 1
        else:
            DIR = PATH_TO_DIR + '/tests/' + str(key_section) + "/" + str(key_method)
            with open(DIR + '_data_successful.json') as data_file:
                data = json.load(data_file)
            # print(row)

            dict_check_dump = data["dict_check_for_output"]
            if dict_check_dump:
                replacement(data=dict_check_dump, keys=dict_check_dump.keys())

            test = baseTest(
                name=data["summary"],
                num=num_test,
                microtest_data=data,
                name_file=name_file,
                type_=data["method"],
                DIR=DIR
            )
            mes_test = test.start()
            num_test += 1

