from pprint import pprint
import json

check_type = {
        list: "list",
        str: "str",
        dict: "dict",
        int: "int",
        bool: "bool"
    }


def req_func(input, output, keys):
    for key in keys:
        type_input = type(input[key])
        if type_input == list:

            output[key] = {"type": "list", "next": []}
            req_func(input=input[key], output=output[key]["next"], keys=range(len(input[key])))

        elif type_input == dict:
            if type(key) == int:
                output.append({"type": "dict", "next": {}})
            else:
                output[key] = {"type": "dict", "next": {}}
            req_func(input=input[key], output=output[key]["next"], keys=input[key].keys())
        else:
            if type(key) == int:
                output.append({"type": check_type[type_input], "next": None})
            else:
                output[key] = {"type": check_type[type_input], "next": None}

input = {"message": [{"id": 1, "name": "test"}, {"id": 2, "name": "test2"}],
               "test": "rr"}


output = {}
keys = input.keys()
req_func(input, output, keys)
pprint(output)
with open('data.txt', 'w') as outfile:
    json.dump(output, outfile)


"""
check_type = {
    "list": list,
    "str": str,
    "dict": dict,
    "int": int
}


dict[key]["type"]


check_type(dict[key]["type"])



input_json = {'message': {
                'type': "dict",
                'next': {
                    'body': {
                                'type': "str",
                                'next': None}, 
                    'title': {
                                'type': "str",
                                'next': None}, 
                    'code': {
                                'type': "str",
                                'next': None}}}, 
'code': {
            'type': "int",
            'next': None}}

"""



# TODO
# обернуть str