import requests
import time
from termcolor import colored


base_url = "http://178.159.110.21:73/exzam/api/mobile/v1"
url_user_entry = "/user/entry/social"
url_user_info = "/user/info"
url_user_update = "/user/update"
url_user_logout = "/user/logout"

url_faculty = "/faculty/university"
url_faculty_course_subject = "/faculty/course/subject"

url_subject_seminar = "/subject/seminars"
url_subject_seminar_questions = "/subject/seminar/questions"
url_subject_seminar_question = "/subject/seminar/question"

url_subject_exams = "/subject/exams"
url_subject_exam_question = "/subject/exam/question"

url_subject_course_works = "/subject/course_works"
url_subject_course_work_order = "/subject/course_work/order"

url_favorites = "/favorites"
url_favorites_with_answer = "/favorites/with_answer"
url_favorite_add = "/favorite/add"
url_favorite_remove = "/favorite/remove"
url_favorite_questions = "/favorite/questions"
url_favorite_drop = "/favorite/drop"


class Token:
    token = None

    def get(self):
        return self.token


# обновление токена из заголовка ответа
def token_update(headers):
    global token
    try:
        token.token = headers["Token"]
    except KeyError:
        print("error except token key error")


class baseWorkWithNetwork:
    # метод POST
    @staticmethod
    def test_post_method(url_method, json_entry, headers):
        start_time = time.time()
        result = requests.post(url=base_url + url_method, json=json_entry, headers=headers)
        end_time = time.time()
        time_work = end_time - start_time
        try:
            return result.json(), time_work, result.headers
        except Exception as e:
            print(e)

    # метод GET
    @staticmethod
    def test_get_method(url_method, headers, params):
        start_time = time.time()
        result = requests.get(url=base_url + url_method, headers=headers, params=params)
        end_time = time.time()
        time_work = end_time - start_time
        return result.json(), time_work, result.headers


class baseMicrotest(baseWorkWithNetwork):
    """
        type    string  POST, GET,
    """
    microtest = {
        "index": None,
        "name": None,
        "status": True,
        "message": None,
        "time_work": None}

    return_headers = None
    f = "\t\t\t"
    __message = "Входные параметры:\n {f} {jE} \n{f}{p} \n{f} Headers:\n {f}{h} \n\n{f} Выходные параметры:\n {f}{rM}" \
                "\n\n {f}{mesResOut}"

    def req_func(self, dict_, result_json, keys):
        res = []
        for key in keys:
            if type(key) == str:
                if dict_.get(key, False):
                    if (type(result_json.get(key, False)) != dict_[key]["type"]) is True:
                        res.append(key)
                    else:
                        if dict_[key]["next"] is not None:
                            if type(result_json[key]) == list:
                                key_next = range(len(result_json[key]))
                            else:
                                key_next = result_json[key].keys()
                            tmp = self.req_func(dict_=dict_[key]["next"], result_json=result_json[key], keys=key_next)
                            if len(tmp) > 0:
                                res += tmp
                else:
                    res.append(key)
            else:
                try:
                    if ((type(result_json[key])) != dict_[0]["type"]) is True:
                        res.append(key)
                    else:
                        if dict_[0]["next"] is not None:
                            if type(result_json[key]) == list:
                                key_next = range(len(result_json[key]))
                            else:
                                key_next = result_json[key].keys()
                            tmp = self.req_func(dict_=dict_[0]["next"], result_json=result_json[key], keys=key_next)
                            if len(tmp) > 0:
                                res += tmp
                except IndexError:
                    res.append(key)
        return res

    def __init__(self, headers, struct_microtest, type, url, response_code, check_for_output, json_entry=False, params=False):
        assert type in ["POST", "GET"]
        self.__clean()
        if headers.get("Token", False):
            try:
                headers["Token"] = headers["Token"]()
            except TypeError:
                pass
            except Exception as e:
                print(e)

        self.headers = headers

        self.microtest["index"] = struct_microtest["index"]
        self.microtest["name"] = struct_microtest["name"]
        self.type = type
        self.url = url
        if not json_entry:
            self.json_entry = ""
        else:
            self.json_entry = json_entry
        if not params:
            self.params = ""
        else:
            self.params = params
        self.__response_code = response_code
        self.check_for_output = check_for_output
        self.message_check_output = ""

    def __clean(self):
        self.microtest = {
            "index": None,
            "name": None,
            "status": True,
            "message": None,
            "time_work": None}
        self.return_headers = None

    def __action(self, action, result, message, mes_res_output):
        if action is None:
            pass
        elif action is True:
            self.microtest["message"] = message.format(
                jE=self.json_entry,
                p=self.params,
                h=self.headers,
                f=self.f,
                rM=result["message"],
                mesResOut=mes_res_output)
        elif action is False:
            self.microtest["message"] = message.format(
                jE=self.json_entry,
                p=self.params,
                f=self.f,
                h=self.headers,
                rM=result["message"],
                mesResOut=mes_res_output)
            self.microtest["status"] = False

    def start(self):
        if self.type == "POST":
            result, self.microtest["time_work"], self.return_headers = self.test_post_method(
                                                                                url_method=self.url,
                                                                                json_entry=self.json_entry,
                                                                                headers=self.headers)
        elif self.type == "GET":
            result, self.microtest["time_work"], self.return_headers = self.test_get_method(
                                                                                    url_method=self.url,
                                                                                    headers=self.headers,
                                                                                    params=self.params)

        # если есть структура для проверки получаемого json, то проверяем
        if self.check_for_output:
            keys = result.keys()
            self.message_check_output = self.req_func(dict_=self.check_for_output, result_json=result, keys=keys)

        resp_code = result["message"]["code"]
        if self.message_check_output:
            self.message_check_output = "Неверный тип получаемого ключа/ключ отсутствует " + str(self.message_check_output) + "\n"
            self.__action(action=False,
                          result=result,
                          message=self.__message,
                          mes_res_output=self.message_check_output)
            # смотрим есть ли доп функция
            if self.__response_code[resp_code]["func"]:
                self.__response_code[resp_code]["func"](headers=self.return_headers)
        else:
            if self.__response_code.get(resp_code, False):
                action = self.__response_code.get(resp_code)
                self.__action(action=action["action"],
                              result=result,
                              message=self.__message,
                              mes_res_output=self.message_check_output)
                # смотрим есть ли доп функция
                if self.__response_code[resp_code]["func"]:
                    self.__response_code[resp_code]["func"](headers=self.return_headers)
            else:  # в том случае если код ответа не найден в нашей входной структуре
                action = self.__response_code.get("else")
                self.__action(action=action["action"],
                              result=result,
                              message="Ответ не соответствует документации.\n\t\t\t" + self.__message,
                              mes_res_output=self.message_check_output)


class baseTest():
    def __init__(self, name, num, microtest_data):
        assert type(num) == int
        assert type(name) == str

        self.name = name
        self.num = num
        self.status = True
        self.microtest_data = microtest_data
        self.data = []

    def __print_format(self):
        if self.status:
            status_all = colored(self.status, "green")
        else:
            status_all = colored(self.status, "red")
        print("test {number}. {name}. Status {status} \n".format(
            number=self.num,
            name=self.name,
            status=status_all))

        for microtest in self.data:
            if microtest["status"]:
                microtest_status = colored(microtest["status"], "green")
            else:
                microtest_status = colored(microtest["status"], "red")
            print("\t\t {index}. {name}. Status {status}. Time {time:.3f}s \n".format(
                index=microtest["index"],
                name=microtest["name"],
                status=microtest_status,
                time=microtest["time_work"]))
            if microtest["message"]:
                print("\t\t\t {message} \n".format(message=microtest["message"]))

    def __save_result(self):
        my_file = open("test_write_file.html", "a", encoding="utf-16")
        if self.status:
            status_all_html = "<font style='color:#00ff00'> {s} </font>".format(s=self.status)
        else:
            status_all_html = "<font style='color:#ff0000'> {s} </font>".format(s=self.status)
        test_string = ("test {number}. {name}. Status {status} </br>".format(
                        number=self.num,
                        name=self.name,
                        status=status_all_html))
        my_file.write(test_string)

        for microtest in self.data:
            if microtest["status"]:
                microtest_string_html = "<font style='color:#00ff00'> {s} </font>".format(s=microtest["status"])
            else:
                microtest_string_html = "<font style='color:#ff0000'> {s} </font>".format(s=microtest["status"])
            microtest_string = ("<span style='padding: 0px 40px;'>{index}. {name}. Status {status}. Time {time:.3f}s</span></br>".format(
                                index=microtest["index"],
                                name=microtest["name"],
                                status=microtest_string_html,
                                time=microtest["time_work"]))
            my_file.write(microtest_string)
            if microtest["message"]:

                mes = microtest["message"].replace("\n", "</br>")
                mes = mes.replace("\t\t\t", "<span style='padding: 0px 40px;'></span>")
                mes_string = ("<span style='padding: 0px 80px;'>{message} </span>".format(message=mes))
                my_file.write(mes_string)
            my_file.write("</br>")

        my_file.close()

    def start(self):
        start = 65  # если количсество микротестов больше 26 - придумать как добавлять АААААА
        for index, input in enumerate(self.microtest_data):
            input["struct_microtest"]["index"] = chr(start + index)
            microtest = baseMicrotest(
                headers=input["headers"],
                struct_microtest=input["struct_microtest"],
                type=input["type"],
                url=input["url"],
                json_entry=input["json_entry"],
                params=input["params"],
                response_code=input["response_code"],
                check_for_output=input["dict_check_for_output"]
            )
            microtest.start()
            if microtest.microtest["status"] is False:
                self.status = False
            self.data.append(microtest.microtest)

        self.__print_format()
        self.__save_result()


token = Token()


def replacement(data, keys):
    check_type = {
        "list": list,
        "str": str,
        "dict": dict,
        "int": int,
        "bool": bool
    }

    for key in keys:
        type_now = data[key]['type']
        data[key]['type'] = check_type[type_now]
        if data[key]['next'] is not None:
            if type(data[key]["next"]) == list:
                keys_next = range(len(data[key]["next"]))
            else:
                keys_next = data[key]["next"].keys()
            replacement(data[key]["next"], keys_next)
    return data


microtest_data =[
    {
        "data": [
            {
                "headers": {"Content_Type": "application/json"},
                "struct_microtest": {"name": "Корректные данные"},
                "type": "POST",
                "url": url_user_entry,
                "json_entry": {
                    "social": "FB",
                    "token_social": "EAAMYGrdEr3UBAOL0f5lUkAdAzyuc2oM6CNKOH9SmKOQPMufNV1t3zTtbHJZAf38Oc4XVXvbTjZCA4PMCuftybIQ7AVUkwmu6bBmbL0OHmBphcgaG0IAPgWjUjUd97ZBntQyjrpZC1yWExYIE2budqW2XPFfeFtOclaZBOiZB4PJYy2N902b2MIussXcLh9nTM0D1Fv9JHyF0ZAHpFFogXKuch8RgWFPalVwjJiuKHUbZAXQOgCIwTen6"},
                "params": {},
                "response_code": {
                    1: {
                        "action": None,
                        "func": token_update},
                    "else": {
                        "action": False,
                        "func": None}},
                "dict_check_for_output": {
                    "data": {
                        "next": {
                            "user": {
                                "next": {
                                    "title_course": {"next": None, "type": "str"},
                                    "id_user": {"next": None, "type": "int"},
                                    "is_update_info": {"next": None, "type": "bool"},
                                    "title_faculty": {"next": None, "type": "str"},
                                    "id_course_faculty": {"next": None, "type": "str"},
                                    "full_name": {"next": None, "type": "str"},
                                    "id_course": {"next": None, "type": "int"},
                                    "title_social": {"next": None, "type": "str"},
                                    "id_faculty": {"next": None, "type": "int"},
                                    "email": {"next": None, "type": "str"},
                                    "id_in_social": {"next": None, "type": "str"},
                                    "is_subscription": {"next": None, "type": "bool"}},
                                "type": "dict"}},
                        "type": "dict"},
                    "message": {
                        "next": {
                            "body": {"next": None, "type": "str"},
                            "code": {"next": None, "type": "int"},
                            "title": {"next": None, "type": "str"}},
                        "type": "dict"},
                    "code": {"next": None, "type": "int"}}
            },
            {
                "headers": { "Content_Type": "application/json"},
                "struct_microtest":{
                    "name": "Невалидный токен социальной сети/невалидная соц. сеть"},
                "type": "POST",
                "url": url_user_entry,
                "json_entry": {
                    "social": "FB",
                    "token_social": "test"},
                "params": {},
                "response_code": {
                    -1: {
                        "action": None,
                        "func": None},
                    1: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": { "Content_Type": "application/json"},
                "struct_microtest":{
                    "name": "Отсутствие необходимого аргумета/пустое значение аргумента"},
                "type": "POST",
                "url": url_user_entry,
                "json_entry": {
                    "social": "FB",
                    "token_social": ""},
                "params": {},
                "response_code": {
                    -33: {
                        "action": None,
                        "func": None},
                    -35: {
                        "action": None,
                        "func": None},
                    1: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            }],
        "name": "Проверка входа"},
    {
        "data": [
             {
                 "headers": {
                     "Content_Type": "application/json",
                     "Token": token.get},
                 "struct_microtest": {
                     "name": "Получение корректных данных по токену"},
                 "type": "GET",
                 "url": url_user_info,
                 "json_entry": {},
                 "params": {},
                 "response_code": {
                     1: {
                         "action": None,
                         "func": None},
                     "else": {
                         "action": False,
                         "func": None}},
                 "dict_check_for_output": {
                     "data": {
                         "type": "dict",
                         "next": {
                             "user": {
                                 "type": "dict",
                                 "next": {
                                     "is_update_info": {"type": "bool", "next": None},
                                     "title_faculty": {"type": "str", "next": None},
                                     "id_course_faculty": {"type": "str", "next": None},
                                     "is_subscription": {"type": "bool", "next": None},
                                     "id_in_social": {"type": "str", "next": None},
                                     "id_faculty": {"type": "int", "next": None},
                                     "id_course": {"type": "int", "next": None},
                                     "full_name": {"type": "str", "next": None},
                                     "email": {"type": "str", "next": None},
                                     "title_social": {"type": "str", "next": None},
                                     "title_course": {"type": "str", "next": None},
                                     "id_user": {"type": "int", "next": None}}}}},
                     "message": {
                         "type": "dict",
                         "next": {
                             "code": {"type": "int", "next": None},
                             "body": {"type": "str", "next": None},
                             "title": {"type": "str", "next": None}}},
                     "code": {"type": "int", "next": None}}
             },
             {
                 "headers": {
                     "Content_Type": "application/json",
                     "Token": "fszf"},
                 "struct_microtest": {"name": "Невалидный токен"},
                 "type": "GET",
                 "url": url_user_info,
                 "json_entry": {},
                 "params": {},
                 "response_code": {
                     -32: {
                         "action": None,
                         "func": None},
                     1: {
                         "action": False,
                         "func": None},
                     "else": {
                         "action": True,
                         "func": None}},
                 "dict_check_for_output": {}
             },
             {
                 "headers": {
                     "Content_Type": "application/json"},
                 "struct_microtest": {"name": "Недостаточно аргументов"},
                 "type": "GET",
                 "url": url_user_info,
                 "json_entry": {},
                 "params": {},
                 "response_code": {
                     -34: {
                         "action": None,
                         "func": None},
                     1: {
                         "action": False,
                         "func": None},
                     "else": {
                         "action": True,
                         "func": None}},
                 "dict_check_for_output": {}
             }],
        "name": "Проверка получения информации о пользователе"},
    {
        "data": [
            {
                "headers": {
                    "Content_Type": "application/json",
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Успешное обновление (входящие параметры корректны)"},
                "type": "POST",
                "url": url_user_update,
                "json_entry": {
                    "id_course_faculty": 30},
                    # "full_name": "string",
                    # "email": ""},
                "params": {},
                "response_code": {
                    1: {
                        "action": None,
                        "func": token_update},
                    "else": {
                        "action": False,
                        "func": None}},
                "dict_check_for_output": {
                    "message": {
                        "type": "dict",
                        "next": {
                            "title": {"type": "str", "next": None},
                            "body": {"type": "str", "next": None},
                            "code": {"type": "int", "next": None}}},
                    "data": {
                        "type": "dict",
                        "next": {
                            "user": {
                                "type": "dict",
                                "next": {
                                    "id_faculty": {"type": "int", "next": None},
                                    "id_in_social": {"type": "str", "next": None},
                                    "id_course": {"type": "int", "next": None},
                                    "title_faculty": {"type": "str", "next": None},
                                    "title_course": {"type": "str", "next": None},
                                    "email": {"type": "str", "next": None},
                                    "id_course_faculty": {"type": "str", "next": None},
                                    "is_subscription": {"type": "bool", "next": None},
                                    "id_user": {"type": "int", "next": None},
                                    "full_name": {"type": "str", "next": None},
                                    "title_social": {"type": "str", "next": None},
                                    "is_update_info": {"type": "bool", "next": None}}}}},
                    "code": {"type": "int", "next": None}}
            },
            {
                "headers": {
                    "Content_Type": "application/json",
                    "Token": "fsdf"},
                "struct_microtest": {"name": "Невалидный токен"},
                "type": "POST",
                "url": url_user_update,
                "json_entry": {
                    "id_course_faculty": 30},
                    # "full_name": "string",
                    # "email": ""},
                "params": {},
                "response_code": {
                    -32: {
                        "action": None,
                        "func": None},
                    1: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {"Content_Type": "application/json"},
                "struct_microtest": {"name": "Недостаточно аргументов"},
                "type": "POST",
                "url": url_user_update,
                "json_entry": {
                    "id_course_faculty": 30},
                    # "full_name": "string",
                    # "email": ""},
                "params": {},
                "response_code": {
                    -33: {
                        "action": None,
                        "func": None},
                    1: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            }],
        "name": "Проверка изменения данных о пользователе"},
    {
        "data": [
            {
                "headers": {"Content_Type": "application/json"},
                "struct_microtest": {
                    "name": "Успешное получение списка факультетов"},
                "type": "GET",
                "url": url_faculty,
                "json_entry": {},
                "params": {"id_university": 1},
                "response_code": {
                    5: {
                    "action": None,
                    "func": None},
                    "else": {
                        "action": False,
                        "func": None}},
                "dict_check_for_output": {
                    "data": {
                        "next": {
                            "faculties": {
                                "next": [
                                    {"next": {
                                        "courses": {
                                            "next": [
                                                {"next": {
                                                    "id_course": {"next": None, "type": "int"},
                                                    "id_course_faculty": {"next": None, "type": "int"},
                                                    "title_course": {"next": None, "type": "str"}},
                                                "type": "dict"}],
                                            "type": "list"},
                                        "id_facuty": {"next": None, "type": "int"},
                                        "title_faculty": {"next": None, "type": "str"},
                                        "path_to_image": {"next": None, "type": "str"}},
                                    "type": "dict"}],
                                "type": "list"}},
                        "type": "dict"},
                    "code": {"next": None, "type": "int"},
                    "message": {
                        "next": {
                            "title": {"next": None, "type": "str"},
                            "code": {"next": None, "type": "int"},
                            "body": {"next": None, "type": "str"}},
                        "type": "dict"}}
            },
            {
                "headers": {"Content_Type": "application/json"},
                "struct_microtest": {
                    "name": "айди университета - строка (неизвестная ошибка)"},
                "type": "GET",
                "url": url_faculty,
                "json_entry": {},
                "params": {"id_university": "i"},
                "response_code": {
                    -31: {
                        "action": None,
                        "func": None},
                    5: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            }],
        "name": "Получение списка факультетов"},
    {
        "data": [
            {
                "headers": {"Token": token.get},
                "struct_microtest": {
                    "name": "Успешное получение списка предметов пользователя"},
                "type": "GET",
                "url": url_faculty_course_subject,
                "json_entry": {},
                "params": {"type_work": "course_work"},
                "response_code": {
                    7: {
                        "action": None,
                        "func": None},
                    "else": {
                        "action": False,
                        "func": None}},
                "dict_check_for_output": {
                    "message": {
                        "next": {
                            "title": {"next": None, "type": "str"},
                            "code": {"next": None, "type": "int"},
                            "body": {"next": None, "type": "str"}},
                        "type": "dict"},
                    "data": {
                        "next": {
                            "subjects": {
                                "next": [{
                                    "next": {
                                        "is_enabled": {"next": None, "type": "bool"},
                                        "title_subject": {"next": None, "type": "str"},
                                        "id_course_subject": {"next": None, "type": "int"}},
                                    "type": "dict"}],
                                "type": "list"},
                            "type_work": {"next": None, "type": "str"}},
                        "type": "dict"},
                    "code": {"next": None, "type": "int"}}
            },
            {
                "headers": {"Token": "fwef"},
                "struct_microtest": {"name": "Невалидный токен"},
                "type": "GET",
                "url": url_faculty_course_subject,
                "json_entry": {},
                "params": {"type_work": "course_work"},
                "response_code": {
                    -32: {
                        "action": None,
                        "func": None},
                    7: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {"Token": token.get},
                "struct_microtest": {
                    "name": "Недостаточно аргументов для выполнения запроса"},
                "type": "GET",
                "url": url_faculty_course_subject,
                "json_entry": {},
                "params": {},
                "response_code": {
                    -33: {
                        "action": None,
                        "func": None},
                    7: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {"Token": token.get},
                "struct_microtest": {"name": "Неверный тип работы"},
                "type": "GET",
                "url": url_faculty_course_subject,
                "json_entry": {},
                "params": {"type_work": "sssfsdfs"},
                "response_code": {
                    -10: {
                        "action": None,
                        "func": None},
                    7: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            }],
        "name": "Получение списка предметов пользователя"},
    {
        "data": [
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Успешное получение списка предметов пользователя"},
                "type": "GET",
                "url": url_subject_seminar,
                "json_entry": {},
                "params": {"id_subject": 38},
                "response_code": {
                    8: {
                        "action": None,
                        "func": None},
                    "else": {
                        "action": False,
                        "func": None}},
                "dict_check_for_output": {
                    "code": {"type": "int", "next": None},
                    "message": {
                        "type": "dict",
                        "next": {
                            "title": {"type": "str", "next": None},
                            "code": {"type": "int", "next": None},
                            "body": {"type": "str", "next": None}}},
                    "data": {
                        "type": "dict",
                        "next": {
                            "seminars": {
                                "type": "list",
                                "next": [{
                                    "type": "dict",
                                    "next": {
                                        "theme_seminar": {"type": "str", "next": None},
                                        "quantity_ask": {"type": "int", "next": None},
                                        "id_seminar": {"type": "int", "next": None}}}]}}}}
            },
            {
                "headers": {
                    "Token": "fwef"},
                "struct_microtest": {
                    "name": "Невалидный токен"},
                "type": "GET",
                "url": url_subject_seminar,
                "json_entry": {},
                "params": {"id_subject": 38},
                "response_code": {
                    -32: {
                        "action": None,
                        "func": None},
                    8: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Предмет, для которого нет семинара"},
                "type": "GET",
                "url": url_subject_seminar,
                "json_entry": {},
                "params": {"id_subject": 54},
                "response_code": {
                    -12: {
                        "action": None,
                        "func": None},
                    8: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Недостаточно аргументов для выполнения запроса"},
                "type": "GET",
                "url": url_subject_seminar,
                "json_entry": {},
                "params": {},
                "response_code": {
                    -33: {
                        "action": None,
                        "func": None},
                    8: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            }],
        "name": "Получение семинаров для курса пользователя"},
    {
        "data": [
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Успешное получение вопросов"
                },
                "type": "GET",
                "url": url_subject_seminar_questions,
                "json_entry": {},
                "params": {"id_seminar": 53},
                "response_code": {
                    9: {
                        "action": None,
                        "func": None},
                    "else": {
                        "action": False,
                        "func": None}},
                "dict_check_for_output": {
                    "message": {
                        "next": {
                            "title": {"next": None, "type": "str"},
                            "code": {"next": None, "type": "int"},
                            "body": {"next": None, "type": "str"}},
                        "type": "dict"},
                    "code": {"next": None, "type": "int"},
                    "data": {
                        "next": {
                            "questions": {
                                "next": [{
                                    "next": {
                                        "title": {"next": None, "type": "str"},
                                        "id_object": {"next": None, "type": "int"},
                                        "hash": {"next": None, "type": "str"}},
                                    "type": "dict"}],
                                "type": "list"},
                            "tasks": {
                                "next": [{
                                    "next": {
                                        "title": {"next": None, "type": "str"},
                                        "id_object": {"next": None, "type": "int"},
                                        "hash": {"next": None, "type": "str"}},
                                    "type": "dict"}],
                                "type": "list"}},
                        "type": "dict"}}
            },
            {
                "headers": {
                    "Token": "fwef"},
                "struct_microtest": {
                    "name": "Невалидный токен"},
                "type": "GET",
                "url": url_subject_seminar_questions,
                "json_entry": {},
                "params": {"id_seminar": 53},
                "response_code": {
                    -32: {
                        "action": None,
                        "func": None},
                    9: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Нет вопросов для выбранного семинара"},
                "type": "GET",
                "url": url_subject_seminar_questions,
                "json_entry": {},
                "params": {"id_seminar": 32},
                "response_code": {
                    -13: {
                        "action": None,
                        "func": None},
                    9: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Недостаточно аргументов для выполнения запроса"},
                "type": "GET",
                "url": url_subject_seminar_questions,
                "json_entry": {},
                "params": {},
                "response_code": {
                    -33: {
                        "action": None,
                        "func": None},
                    9: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            }],
        "name": "Получение вопросов семинара"},
    {
        "data": [
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Успешное получение вопроса"
                },
                "type": "GET",
                "url": url_subject_seminar_question,
                "json_entry": {},
                "params": {"id_question": 95},
                "response_code": {
                    10: {
                        "action": None,
                        "func": None},
                    "else": {
                        "action": False,
                        "func": None}},
                "dict_check_for_output": {
                    "message": {
                        "next": {
                            "code": {"next": None, "type": "int"},
                            "body": {"next": None, "type": "str"},
                            "title": {"next": None, "type": "str"}},
                        "type": "dict"},
                    "code": {"next": None, "type": "int"},
                    "data": {
                        "next": {
                            "question": {
                                "next": {
                                    "id_object": {"next": None, "type": "int"},
                                    "answer": {"next": None, "type": "str"},
                                    "hash": {"next": None, "type": "str"},
                                    "title": {"next": None, "type": "str"},
                                    "type": {"next": None, "type": "str"}},
                                "type": "dict"}},
                        "type": "dict"}}
            },
            {
                "headers": {
                    "Token": "fwef"},
                "struct_microtest": {
                    "name": "Невалидный токен"},
                "type": "GET",
                "url": url_subject_seminar_question,
                "json_entry": {},
                "params": {"id_question": 95},
                "response_code": {
                    -32: {
                        "action": None,
                        "func": None},
                    10: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "По выбранному id вопрос не найден"},
                "type": "GET",
                "url": url_subject_seminar_question,
                "json_entry": {},
                "params": {"id_question": 2},
                "response_code": {
                    -14: {
                        "action": None,
                        "func": None},
                    10: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Недостаточно аргументов для выполнения запроса"},
                "type": "GET",
                "url": url_subject_seminar_question,
                "json_entry": {},
                "params": {},
                "response_code": {
                    -33: {
                        "action": None,
                        "func": None},
                    10: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            }],
        "name": "Получение вопроса семинара"},
    {
        "data": [
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Успешное получение вопросов"
                },
                "type": "GET",
                "url": url_subject_exams,
                "json_entry": {},
                "params": {"id_subject": 34},
                "response_code": {
                    11: {
                        "action": None,
                        "func": None},
                    "else": {
                        "action": False,
                        "func": None}},
                "dict_check_for_output": {
                    "message": {
                        "type": "dict",
                        "next": {
                            "title": {"type": "str", "next": None},
                            "body": {"type": "str", "next": None},
                            "code": {"type": "int", "next": None}}},
                    "code": {"type": "int", "next": None},
                    "data": {
                        "type": "dict",
                        "next": {
                            "exams": {
                                "type": "list",
                                "next": [{
                                    "type": "dict",
                                    "next": {
                                        "title": {"type": "str", "next": None},
                                        "id_exam": {"type": "int", "next": None},
                                        "hash": {"type": "str", "next": None}}}]}}}}
            },
            {
                "headers": {
                    "Token": "fwef"},
                "struct_microtest": {
                    "name": "Невалидный токен"},
                "type": "GET",
                "url": url_subject_exams,
                "json_entry": {},
                "params": {"id_subject": 95},
                "response_code": {
                    -32: {
                        "action": None,
                        "func": None},
                    11: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Нет вопросов для выбранного экзамена"},
                "type": "GET",
                "url": url_subject_exams,
                "json_entry": {},
                "params": {"id_subject": 2},
                "response_code": {
                    -415: {
                        "action": None,
                        "func": None},
                    11: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Недостаточно аргументов для выполнения запроса"},
                "type": "GET",
                "url": url_subject_exams,
                "json_entry": {},
                "params": {},
                "response_code": {
                    -33: {
                        "action": None,
                        "func": None},
                    11: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            }],
        "name": "Получение вопросов экзамена"},
    {
        "data": [
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Успешное получение вопроса"
                },
                "type": "GET",
                "url": url_subject_exam_question,
                "json_entry": {},
                "params": {"id_question_exam": 40},
                "response_code": {
                    12: {
                        "action": None,
                        "func": None},
                    "else": {
                        "action": False,
                        "func": None}},
                "dict_check_for_output": {
                    "data": {
                        "type": "dict",
                        "next": {
                            "exam_question": {
                                "type": "dict",
                                "next": {
                                    "hash": {"type": "str", "next": None},
                                    "answer": {"type": "str", "next": None},
                                    "id_object": {"type": "int", "next": None},
                                    "title": {"type": "str", "next": None}}}}},
                    "message": {
                        "type": "dict",
                        "next": {
                            "title": {"type": "str", "next": None},
                            "code": {"type": "int", "next": None},
                            "body": {"type": "str", "next": None}}},
                    "code": {"type": "int", "next": None}}
            },
            {
                "headers": {
                    "Token": "fwef"},
                "struct_microtest": {
                    "name": "Невалидный токен"},
                "type": "GET",
                "url": url_subject_exam_question,
                "json_entry": {},
                "params": {"id_question_exam": 40},
                "response_code": {
                    -32: {
                        "action": None,
                        "func": None},
                    12: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "По выбранному id вопрос не найден"},
                "type": "GET",
                "url": url_subject_exam_question,
                "json_entry": {},
                "params": {"id_question_exam": 2},
                "response_code": {
                    -16: {
                        "action": None,
                        "func": None},
                    12: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Недостаточно аргументов для выполнения запроса"},
                "type": "GET",
                "url": url_subject_exam_question,
                "json_entry": {},
                "params": {},
                "response_code": {
                    -35: {
                        "action": None,
                        "func": None},
                    12: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            }],
        "name": "Получение вопроса экзамена"},
    {
        "data": [
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Успешное получение курсовых"
                },
                "type": "GET",
                "url": url_subject_course_works,
                "json_entry": {},
                "params": {"id_subject": 34},
                "response_code": {
                    13: {
                        "action": None,
                        "func": None},
                    "else": {
                        "action": False,
                        "func": None}},
                "dict_check_for_output": {
                    "code": {"type": "int", "next": None},
                    "data": {
                        "type": "dict",
                        "next": {
                            "course_works": {
                                "type": "list",
                                "next": [{
                                    "type": "dict",
                                    "next": {
                                        "id_course_work": {"type": "int", "next": None},
                                        "title": {"type": "str", "next": None},
                                        "description": {"type": "str", "next": None},
                                        "price": {"type": "int", "next": None}}}]}}},
                    "message": {
                        "type": "dict",
                        "next": {
                            "body": {"type": "str", "next": None},
                            "code": {"type": "int", "next": None},
                            "title": {"type": "str", "next": None}}}}
            },
            {
                "headers": {
                    "Token": "fwef"},
                "struct_microtest": {
                    "name": "Невалидный токен"},
                "type": "GET",
                "url": url_subject_course_works,
                "json_entry": {},
                "params": {"id_subject": 34},
                "response_code": {
                    436: {
                        "action": None,
                        "func": None},
                    13: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Недостаточно аргументов для выполнения запроса"},
                "type": "GET",
                "url": url_subject_course_works,
                "json_entry": {},
                "params": {},
                "response_code": {
                    437: {
                        "action": None,
                        "func": None},
                    13: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            }],
        "name": "Получение списка курсовых"},
    {
        "data": [
            {
                "headers": {
                    "Content_Type": "application/json",
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Успешное оформление заказа"},
                "type": "POST",
                "url": url_subject_course_work_order,
                "json_entry": {
                    "id_theme": 58,
                    "email": "de@gmail.com",
                    "phone": "string",
                    "name": "deniska",
                    "text": "string"},
                "params": {},
                "response_code": {
                    14: {
                        "action": None,
                        "func": None},
                    "else": {
                        "action": False,
                        "func": None}},
                "dict_check_for_output": {
                    "code": {"type": "int", "next": None},
                    "message": {
                        "type": "dict",
                        "next": {
                            "title": {"type": "str", "next": None},
                            "code": {"type": "int", "next": None},
                            "body": {"type": "str", "next": None}}}}
            },
            {
                "headers": {
                    "Content_Type": "application/json",
                    "Token": "fsdf"},
                "struct_microtest": {"name": "Невалидный токен"},
                "type": "POST",
                "url": url_subject_course_work_order,
                "json_entry": {
                    "id_theme": 58,
                    "email": "de@gmail.com",
                    "phone": "string",
                    "name": "deniska",
                    "text": "string"},
                "params": {},
                "response_code": {
                    -32: {
                        "action": None,
                        "func": None},
                    14: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
             },
            {
                "headers": {
                    "Content_Type": "application/json",
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Недостаточно аргументов для выполнения запроса"},
                "type": "POST",
                "url": url_subject_course_work_order,
                "json_entry": {
                    "email": "de@gmail.com",
                    "phone": "string",
                    "name": "deniska",
                    "text": "string"},
                "params": {},
                "response_code": {
                    -33: {
                        "action": None,
                        "func": None},
                    14: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            }],
        "name": "Оформление заказа курсовой"},
    {
        "data": [
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Успешное получение избранных вопросов"
                },
                "type": "GET",
                "url": url_favorites,
                "json_entry": {},
                "params": {"is_seminar_question": True},
                "response_code": {
                    17: {
                        "action": None,
                        "func": None},
                    "else": {
                        "action": False,
                        "func": None}},
                "dict_check_for_output": {
                    "code": {"type": "int", "next": None},
                    "data": {
                        "type": "dict",
                        "next": {
                            "favorites": {
                                "type": "dict",
                                "next": {
                                    "favorite_seminar": {
                                        "type": "list",
                                        "next": [{
                                            "type": "dict",
                                            "next": {
                                                "hash": {"type": "str", "next": None},
                                                "theme_question": {"type": "str", "next": None},
                                                "id_question": {"type": "int", "next": None},
                                                "title_subject": {"type": "str", "next": None}}}]},
                                    "favorite_exam": {
                                        "type": "list",
                                        "next": [{
                                            "type": "dict",
                                            "next": {
                                                "hash": {"type": "str", "next": None},
                                                "theme_question": {"type": "str", "next": None},
                                                "id_question": {"type": "int", "next": None},
                                                "title_subject": {"type": "str", "next": None}}}]}}}}},
                    "message": {
                        "type": "dict",
                        "next": {
                            "code": {"type": "int", "next": None},
                            "title": {"type": "str", "next": None},
                            "body": {"type": "str", "next": None}}}}
            },
            {
                "headers": {
                    "Token": "fwef"},
                "struct_microtest": {
                    "name": "Невалидный токен"},
                "type": "GET",
                "url": url_favorites,
                "json_entry": {},
                "params": {"is_seminar_question": True},
                "response_code": {
                    -32: {
                        "action": None,
                        "func": None},
                    17: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {},
                "struct_microtest": {
                    "name": "Недостаточно аргументов для выполнения запроса"},
                "type": "GET",
                "url": url_favorites,
                "json_entry": {},
                "params": {"is_seminar_question": True},
                "response_code": {
                    -33: {
                        "action": None,
                        "func": None},
                    17: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            }],
        "name": "Получение списка избранного"},
{
        "data": [
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Успешное получение избранных вопросов с ответом"
                },
                "type": "GET",
                "url": url_favorites_with_answer,
                "json_entry": {},
                "params": {"is_seminar_question": True},
                "response_code": {
                    17: {
                        "action": None,
                        "func": None},
                    "else": {
                        "action": False,
                        "func": None}},
                "dict_check_for_output": {
                    "message":{
                        "next": {
                            "title": {"next": None, "type": "str"},
                            "code": {"next": None, "type": "int"},
                            "body": {"next": None, "type": "str"}},
                        "type": "dict"},
                    "code": {"next": None, "type": "int"},
                    "data": {
                        "next": {
                            "favorites": {
                                "next": {
                                    "favorite_seminar": {
                                        "next": [{
                                            "next": {
                                                "id_question": {"next": None, "type": "int"},
                                                "answer": {"next": None, "type": "str"},
                                                "theme_question": {"next": None, "type": "str"},
                                                "hash": {"next": None, "type": "str"},
                                                "title_subject": {"next": None, "type": "str"}},
                                            "type": "dict"}],
                                        "type": "list"},
                                    "favorite_exam": {
                                        "next": [{
                                            "next": {
                                                "id_question": {"next": None, "type": "int"},
                                                "answer": {"next": None, "type": "str"},
                                                "theme_question": {"next": None, "type": "str"},
                                                "hash": {"next": None, "type": "str"},
                                                "title_subject": {"next": None, "type": "str"}},
                                            "type": "dict"}],
                                        "type": "list"}},
                                "type": "dict"}},
                        "type": "dict"}}
            },
            {
                "headers": {
                    "Token": "fwef"},
                "struct_microtest": {
                    "name": "Невалидный токен"},
                "type": "GET",
                "url": url_favorites_with_answer,
                "json_entry": {},
                "params": {"is_seminar_question": True},
                "response_code": {
                    -32: {
                        "action": None,
                        "func": None},
                    17: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {},
                "struct_microtest": {
                    "name": "Недостаточно аргументов для выполнения запроса"},
                "type": "GET",
                "url": url_favorites_with_answer,
                "json_entry": {},
                "params": {"is_seminar_question": True},
                "response_code": {
                    -33: {
                        "action": None,
                        "func": None},
                    17: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            }],
        "name": "Получение списка избранного с ответом"},
    {
        "data": [
            {
                "headers": {
                    "Content_Type": "application/json",
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Успешное добавление вопроса в избранное"},
                "type": "POST",
                "url": url_favorite_add,
                "json_entry": {
                    "is_seminar_question": True,
                    "id_question": 134,
                    "is_answer_needed": True},
                "params": {},
                "response_code": {
                    15: {
                        "action": None,
                        "func": None},
                    "else": {
                        "action": False,
                        "func": None}},
                "dict_check_for_output": {
                    "data": {
                        "type": "dict",
                        "next": {
                            "question": {
                                "type": "dict",
                                "next": {
                                    "hash": {"type": "str", "next": None},
                                    "theme_question": {"type": "str", "next": None},
                                    "title_subject": {"type": "str", "next": None},
                                    "id_question": {"type": "int", "next": None}}}}},
                    "message": {
                        "type": "dict",
                        "next": {
                            "title": {"type": "str", "next": None},
                            "code": {"type": "int", "next": None},
                            "body": {"type": "str", "next": None}}},
                    "code": {"type": "int", "next": None}}
            },
            {
                "headers": {
                    "Content_Type": "application/json",
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Добавление в избранное вопроса, уже существующего в списке"},
                "type": "POST",
                "url": url_favorite_add,
                "json_entry": {
                    "is_seminar_question": True,
                    "id_question": 134,
                    "is_answer_needed": True},
                "params": {},
                "response_code": {
                    -18: {
                        "action": None,
                        "func": None},
                    15: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {
                    "Content_Type": "application/json",
                    "Token": "fsdf"},
                "struct_microtest": {"name": "Невалидный токен"},
                "type": "POST",
                "url": url_favorite_add,
                "json_entry": {
                    "is_seminar_question": True,
                    "id_question": 134,
                    "is_answer_needed": True},
                "params": {},
                "response_code": {
                    -32: {
                        "action": None,
                        "func": None},
                    15: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {
                    "Content_Type": "application/json",
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Недостаточно аргументов для выполнения запроса"},
                "type": "POST",
                "url": url_favorite_add,
                "json_entry": {
                    "id_question": 134},
                "params": {},
                "response_code": {
                    -33: {
                        "action": None,
                        "func": None},
                    15: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            }],
        "name": "Добавление в избранное"},
    {
        "data": [
            {
                "headers": {
                    "Content_Type": "application/json",
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Успешное получение вопросов по списку id"},
                "type": "POST",
                "url": url_favorite_questions,
                "json_entry": {
                    "is_seminar_question": True,
                    "list_questions": [
                        134,
                        32]},
                "params": {},
                "response_code": {
                    18: {
                        "action": None,
                        "func": None},
                    "else": {
                        "action": False,
                        "func": None}},
                "dict_check_for_output": {
                    "code": {"next": None, "type": "int"},
                    "data": {
                        "next": {
                            "questions": {
                                "next": [{
                                    "next": {
                                        "title": {"next": None, "type": "str"},
                                        "answer": {"next": None, "type": "str"},
                                        "hash": {"next": None, "type": "str"},
                                        "id_object": {"next": None, "type": "int"}},
                                    "type": "dict"}],
                                "type": "list"}},
                        "type": "dict"},
                    "message": {
                        "next": {
                            "code": {"next": None, "type": "int"},
                            "title": {"next": None, "type": "str"},
                            "body": {"next": None, "type": "str"}},
                        "type": "dict"}}
            },
            {
                "headers": {
                    "Content_Type": "application/json",
                    "Token": "fsdf"},
                "struct_microtest": { "name": "Невалидный токен"},
                "type": "POST",
                "url": url_favorite_questions,
                "json_entry": {
                    "is_seminar_question": True,
                    "list_questions": [
                        134]},
                "params": {},
                "response_code": {
                    -32: {
                        "action": None,
                        "func": None},
                    18: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {
                    "Content_Type": "application/json",
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Недостаточно аргументов для выполнения запроса"},
                "type": "POST",
                "url": url_favorite_questions,
                "json_entry": {
                    "list_questions": [
                        134]},
                "params": {},
                "response_code": {
                    -33: {
                        "action": None,
                        "func": None},
                    18: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {
                    "Content_Type": "application/json",
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Список доступных вопросов пуст"},
                "type": "POST",
                "url": url_favorite_questions,
                "json_entry": {
                    "list_questions": [
                        534534]},
                "params": {},
                "response_code": {
                    -21: {
                        "action": None,
                        "func": None},
                    18: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            }],
        "name": "Получение вопросов по списку id (в документации нет описания ошибок)"},
    {
        "data": [
            {
                "headers": {
                    "Content_Type": "application/json",
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Успешное удаление указанного вопроса из избранного"},
                "type": "POST",
                "url": url_favorite_remove,
                "json_entry": {
                    "is_seminar_question": True,
                    "list_questions": [
                        134]},
                "params": {},
                "response_code": {
                    16: {
                        "action": None,
                        "func": None},
                    "else": {
                        "action": False,
                        "func": None}},
                "dict_check_for_output": {
                    "code": {"next": None, "type": "int"},
                    "message": {
                        "next": {
                            "code": {"next": None, "type": "int"},
                            "title": {"next": None, "type": "str"},
                            "body": {"next": None, "type": "str"}},
                        "type": "dict"}}
            },
            {
                "headers": {
                    "Content_Type": "application/json",
                    "Token": "fsdf"},
                "struct_microtest": {"name": "Невалидный токен"},
                "type": "POST",
                "url": url_favorite_remove,
                "json_entry": {
                    "is_seminar_question": True,
                    "list_questions": [
                        134]},
                "params": {},
                "response_code": {
                    -32: {
                        "action": None,
                        "func": None},
                    16: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {
                    "Content_Type": "application/json",
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Недостаточно аргументов для выполнения запроса"},
                "type": "POST",
                "url": url_favorite_remove,
                "json_entry": {
                    "list_questions": [
                        134]},
                "params": {},
                "response_code": {
                    -33: {
                        "action": None,
                        "func": None},
                    16: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            }],
        "name": "Удаление указанного вопроса из избранного"},
    {
        "data": [
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Успешное удаление избранного"},
                "type": "GET",
                "url": url_favorite_drop,
                "json_entry": {},
                "params": {},
                "response_code": {
                    16: {
                        "action": None,
                        "func": None},
                    "else": {
                        "action": False,
                        "func": None}},
                "dict_check_for_output": {
                    "code": {"next": None, "type": "int"},
                    "message": {
                        "next": {
                            "code": {"next": None, "type": "int"},
                            "title": {"next": None, "type": "str"},
                            "body": {"next": None, "type": "str"}},
                        "type": "dict"},
                    "data": {"next": {}, "type": "dict"}}
            },
            {
                "headers": {
                    "Token": "fwef"},
                "struct_microtest": {
                    "name": "Невалидный токен"},
                "type": "GET",
                "url": url_favorite_drop,
                "json_entry": {},
                "params": {},
                "response_code": {
                    -32: {
                        "action": None,
                        "func": None},
                    16: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {},
                "struct_microtest": {
                    "name": "Недостаточно аргументов для выполнения запроса"},
                "type": "GET",
                "url": url_favorite_drop,
                "json_entry": {},
                "params": {},
                "response_code": {
                    -33: {
                        "action": None,
                        "func": None},
                    16: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            }],
        "name": "Удаление всего избранного (в документации нет описания ошибок)"},
    {
        "data": [
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Успешный выход"},
                "type": "GET",
                "url": url_user_logout,
                "json_entry": {},
                "params": {},
                "response_code": {
                    2: {
                        "action": None,
                        "func": None},
                    "else": {
                        "action": False,
                        "func": None}},
                "dict_check_for_output": {
                    "message": {
                        "next": {
                            "body": {"next": None, "type": "str"}, 
                            "code": {"next": None, "type": "int"}, 
                            "title": {"next": None, "type": "str"}}, 
                        "type": "dict"}, 
                    "code": {"next": None, "type": "int"}}
            },
            {
                "headers": {
                    "Token": "fwef"},
                "struct_microtest": {
                    "name": "Невалидный токен (нет в документации)"},
                "type": "GET",
                "url": url_user_logout,
                "json_entry": {},
                "params": {},
                "response_code": {
                    -32: {
                        "action": None,
                        "func": None},
                    2: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {},
                "struct_microtest": {
                    "name": "Недостаточно аргументов для выполнения запроса"},
                "type": "GET",
                "url": url_user_logout,
                "json_entry": {},
                "params": {},
                "response_code": {
                    -33: {
                        "action": None,
                        "func": None},
                    2: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            }],
        "name": "Проверка выхода"}
]
for row in range(len(microtest_data)):
    for i in range(len(microtest_data[row]["data"])):
        data = microtest_data[row]["data"][i]["dict_check_for_output"]
        if data:
            replacement(data=data, keys=data.keys())

#  для проверки того что данные которые мы ожидаем коректны по структуре
def check_input_data(data):
    assert type(data) == list
    for test in data:
        assert type(test) == dict
        assert type(test["name"]) == str
        for microtest in test["data"]:
            assert type(microtest) == dict

            assert type(microtest["headers"]) == dict
            assert type(microtest["json_entry"]) == dict
            assert type(microtest["response_code"]) == dict
            assert type(microtest["dict_check_for_output"]) == dict
            assert type(microtest["type"]) == str
            assert microtest["type"] in ["POST", "GET"]


check_input_data(data=microtest_data)
open("test_write_file.html","w").close()
for num_test, test in enumerate(microtest_data):

    test = baseTest(
        name=test["name"],
        num=num_test+1,
        microtest_data=test["data"])
    test.start()

"""
# test 1. проверка входа
name_test = "Проверка входа"
num_test = 1
status_all = True
data = []

microtest_data = [
    {
        "headers": {
            "Content_Type": "application/json",
            "Token": token},
        "struct_microtest": {
            "name": "Корректные данные"
        },
        "type": "POST",
        "url": url_user_entry,
        "json_entry": {
            "social": "FB",
            "token_social": "EAAMYGrdEr3UBAOL0f5lUkAdAzyuc2oM6CNKOH9SmKOQPMufNV1t3zTtbHJZAf38Oc4XVXvbTjZCA4PMCuftybIQ7AVUkwmu6bBmbL0OHmBphcgaG0IAPgWjUjUd97ZBntQyjrpZC1yWExYIE2budqW2XPFfeFtOclaZBOiZB4PJYy2N902b2MIussXcLh9nTM0D1Fv9JHyF0ZAHpFFogXKuch8RgWFPalVwjJiuKHUbZAXQOgCIwTen6"},
        "response_code": {
            1 : {
                "action": None,
                "func": token_update},
            "else": {
                "action": False,
                "func": None}
        }
    },
    {
        "headers": {
            "Content_Type": "application/json"},
        "struct_microtest":{
            "name": "Невалидный токен социальной сети/невалидная соц. сеть"
        },
        "type": "POST",
        "url": url_user_entry,
        "json_entry": {
            "social": "FB",
            "token_social": "test"},
        "response_code": {
            -1 : {
                "action": None,
                "func": None},
            1 : {
                "action": False,
                "func": None},
            "else": {
                "action": True,
                "func": None}
        }
    },
    {
        "headers": {
            "Content_Type": "application/json"},
        "struct_microtest":{
            "name": "Отсутствие необходимого аргумета/пустое значение аргумента"
        },
        "type": "POST",
        "url": url_user_entry,
        "json_entry": {
            "social": "FB",
            "token_social": ""},
        "response_code": {
            -33 : {
                "action": None,
                "func": None},
            -35 : {
                "action": None,
                "func": None},
            1 : {
                "action": False,
                "func": None},
            "else": {
                "action": True,
                "func": None}
        }
    }
]

start = 65  # если количсество микротестов больше 26 - придумать как добавлять АААААА  
for index, input in enumerate(microtest_data):
    input["struct_microtest"]["index"] = chr(start+index)
    microtest = baseMicrotest(
        headers=input["headers"],
        struct_microtest=input["struct_microtest"],
        type=input["type"],
        url=input["url"],
        json_entry=input["json_entry"],
        response_code=input["response_code"]
    )
    microtest.start()
    if microtest.microtest["status"] is False:
        status_all = False
    data.append(microtest.microtest)

print_format(num_test, name_test, status_all, data)



"""
########################################################################################################################


# вывод результата теста
def print_format(test_number, test_name, status,
                 data):  # data [{"name": "some name", "status": True, "message":None}]
    if status:
        status = colored(status, "green")
    else:
        status = colored(status, "red")
    print("test {number}. {name}. Status {status} \n".format(
        number=test_number,
        name=test_name,
        status=status))

    for microtest in data:
        if microtest["status"]:
            microtest["status"] = colored(microtest["status"], "green")
        else:
            microtest["status"] = colored(microtest["status"], "red")
        print("\t\t {index}. {name}. Status {status}. Time {time:.3f}s \n".format(
            index=microtest["index"],
            name=microtest["name"],
            status=microtest["status"],
            time=microtest["time_work"]))
        if microtest["message"]:
            print("\t\t\t {message} \n".format(message=microtest["message"]))


# метод POST
def test_post_method(url_method, json_entry, headers):

    start_time = time.time()
    result = requests.post(url=base_url + url_method, json=json_entry, headers=headers)
    end_time = time.time()
    time_work = end_time - start_time
    return result, time_work


# метод GET
def test_get_method(url_method, headers, params):

    start_time = time.time()
    result = requests.get(url=base_url + url_method, headers=headers, params=params)
    end_time = time.time()
    time_work = end_time - start_time
    return result, time_work


def struct_microtest(index, name):
    microtest = {
        "index": index,
        "name": name,
        "status": True,
        "message": None,
        "time_work": None}
    return microtest



"""
# test 1. проверка входа
name_test = "Проверка входа"
num_test = 1
status_all = True
data = []

# test 1.A
json_entry = {
    "social": "FB",
    "token_social": "EAAMYGrdEr3UBAOL0f5lUkAdAzyuc2oM6CNKOH9SmKOQPMufNV1t3zTtbHJZAf38Oc4XVXvbTjZCA4PMCuftybIQ7AVUkwmu6bBmbL0OHmBphcgaG0IAPgWjUjUd97ZBntQyjrpZC1yWExYIE2budqW2XPFfeFtOclaZBOiZB4PJYy2N902b2MIussXcLh9nTM0D1Fv9JHyF0ZAHpFFogXKuch8RgWFPalVwjJiuKHUbZAXQOgCIwTen6"}
headers = {"Content_Type": "application/json"}

microtest = struct_microtest(index="A", name="Корректные данные")

result, microtest["time_work"] = test_post_method(url_user_entry, json_entry, headers)
result_json = result.json()

token = result.headers["Token"]
if result_json["message"]["code"] == 1:
    pass
else:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры: \n{f} {jsonEntry} \n\n{f} Выходные параметры: \n{f} {rM}".format(
        jsonEntry=json_entry,
        rM=result_json["message"], # rM resultMessage
        f="\t\t\t")

data.append(microtest)

# test 1.B
json_entry = {
    "social": "FB",
    "token_social": "rwerwgd"}
headers = {"Content_Type": "application/json"}

microtest = struct_microtest("B", "Невалидный токен социальной сети/невалидная соц. сеть")

result, microtest["time_work"] = test_post_method(url_user_entry, json_entry, headers)
result_json = result.json()
if result_json["message"]["code"] == -1:
    pass
elif result_json["message"]["code"] == 1:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры: \n{f} {jsonEntry} \n\n{f} Выходные параметры: \n{f} {rM}".format(
        jsonEntry=json_entry,
        rM=result_json["message"],  # rM resultMessage
        f="\t\t\t")
else:
    microtest["message"] = "Ответ не соответствует документации.\n{f}Входные параметры:\n{f}{jE}\n\n{f} Выходные " \
                           "параметры: \n{f}{rM}".format(
                                                    jE=json_entry,  # jE jsonEntry
                                                    rM=result["message"],  # rM resultMessage
                                                    f="\t\t\t")

data.append(microtest)

# test 1.C
json_entry = {
    "social": "FB",
    "token_social": ""}
headers = {"Content_Type": "application/json"}

microtest = struct_microtest("C", "Отсутствие необходимого аргумета/пустое значение аргумента")

result, microtest["time_work"] = test_post_method(url_user_entry, json_entry, headers)
result_json = result.json()

if result_json["message"]["code"] == -33 or result_json["message"]["code"] == -35:
    pass
elif result_json["message"]["code"] == 1:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры: \n{f} {jsonEntry} \n\n{f} Выходные параметры: \n{f} {rM}".format(
        jsonEntry=json_entry,
        rM=result_json["message"],  # rM resultMessage
        f="\t\t\t")
else:
    microtest["message"] = "Ответ не соответствует документации.\n{f}Входные параметры:\n{f}{jE}\n\n{f} Выходные " \
                           "параметры: \n{f}{rM}".format(
                                                    jE=json_entry,  # jE jsonEntry
                                                    rM=result_json["message"],  # rM resultMessage
                                                    f="\t\t\t")

data.append(microtest)

print_format(num_test, name_test, status_all, data)

# test 2. получение информации о пользователе
name_test = "Проверка получения информации о пользователе"
num_test = 2
status_all = True
data = []
# test 2.A
headers = {"Token": token}

microtest = struct_microtest("A", "Получение корректных данных по токену")

result, microtest["time_work"] = test_get_method(url_user_info, headers)
result_json = result.json()

if result_json["message"]["code"] == 1:
   pass
else:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры (headers): \n{f} {headers} \n\n{f} Выходные параметры: \n{f} {rM}".format(
        headers=headers,
        rM=result_json["message"], # rM resultMessage
        f="\t\t\t")

data.append(microtest)

# test 2.B
headers = {"Token": "SJ9.0CCuinLEX8FHPWYhdo1XErcwiq4hPBtpK20DCNOSGzA "}

microtest = struct_microtest("B","Невалидный токен")

result, microtest["time_work"] = test_get_method(url_user_info, headers)
result_json = result.json()

if result_json["message"]["code"] == -32:
    pass
elif result_json["message"]["code"] == 1:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры (headers): \n{f} {headers} \n\n{f} Выходные параметры: \n{f} {rM}".format(
        headers=headers,
        rM=result_json["message"], # rM resultMessage
        f="\t\t\t")
else:
    microtest["message"] = "Ответ не соответствует документации.\n{f} Входные параметры (headers): \n{f}{headers}\n\n" \
                           "{f} Выходные параметры: \n{f}{rM}".format(
                                                                headers=headers,
                                                                rM=result_json["message"],  # rM resultMessage
                                                                f="\t\t\t")

data.append(microtest)

# test 2.C
headers = {}

microtest = struct_microtest("C", "Недостаточно аргументов")

result, microtest["time_work"] = test_get_method(url_user_info, headers)
result_json = result.json()

if result_json["message"]["code"] == -34:
    pass
elif result_json["message"]["code"] == 1:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры (headers): \n{f} {headers} \n\n{f} Выходные параметры: \n{f} {rM}".format(
        headers=headers,
        rM=result_json["message"],  # rM resultMessage
        f="\t\t\t")
else:
    microtest["message"] = "Ответ не соответствует документации.\n{f} Входные параметры (headers): \n{f}{headers}\n\n "\
                           "{f} Выходные параметры: \n{f}{rM}".format(
                                                                headers=headers,
                                                                rM=result_json["message"],  # rM resultMessage
                                                                f="\t\t\t")

data.append(microtest)

print_format(num_test, name_test, status_all, data)

# test 3. изменение данных о пользователе
name_test = "Проверка изменения данных о пользователе"
num_test = 3
status_all = True
data = []
# test 3.A
# входящий json с любым из представленных аргументов
json_entry = {
    "id_course_faculty": 30}
    # "full_name": "string",
    # "email": ""}
headers = {"Token": token}

microtest = struct_microtest("A", "Успешное обновление (входящие параметры корректны)")

result, microtest["time_work"] = test_post_method(url_user_update, json_entry, headers)
result_json = result.json()

# print(result)
if result_json["message"]["code"] == 1:
    token = result.headers["Token"]
else:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {jE} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные параметры:" \
                           " \n{f} {rM}".format(
        jE=json_entry,    # jE jsonEntry
        headers=headers,
        rM=result_json["message"],  # rM resultMessage
        f="\t\t\t")

data.append(microtest)

# test 3.B
# входящий json с любым из представленных аргументов
json_entry = {
    # "id_course_faculty": 30}
     "full_name": "deniska"}
    # "email": "string"}
headers = {"Token": "reeeqweqw"}

microtest = struct_microtest("B", "Невалидный токен")

result, microtest["time_work"] = test_post_method(url_user_update, json_entry, headers)
result_json = result.json()
# print(result)
if result_json["message"]["code"] == -32:
    pass
elif result_json["message"]["code"] == 1:
    microtest["status"] = False
    status_all = False
    token = result.headers["Token"]
    microtest["message"] = "Входные параметры:\n{f} {jE} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные параметры:" \
                           " \n{f} {rM}".format(
        jE=json_entry,    # jE jsonEntry
        headers=headers,
        rM=result_json["message"],  # rM resultMessage
        f="\t\t\t")
else:
    microtest["message"] = "Ответ не соответствует документации.\n{f} Входные параметры (headers): \n{f}{headers}\n\n" \
                           "{f} Выходные параметры: \n{f}{rM}".format(
                                                                    headers=headers,
                                                                    rM=result_json["message"],  # rM resultMessage
                                                                    f="\t\t\t")

data.append(microtest)

# test 3.C
# входящий json с любым из представленных аргументов
json_entry = {
    # "id_course_faculty": 30}
     "full_name": "deniska"}
    # "email": "string"}
headers = {}    # {"Token": new_token}

microtest = struct_microtest("C", "Недостаточно аргументов")

result, microtest["time_work"] = test_post_method(url_user_update, json_entry, headers)
result_json = result.json()
# print(result)
if result_json["message"]["code"] == -33:
    pass
elif result_json["message"]["code"] == 1:
    microtest["status"] = False
    status_all = False
    token = result.headers["Token"]
    microtest["message"] = "Входные параметры:\n{f} {jE} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные параметры:" \
                           "\n{f} {rM}".format(
                                            jE=json_entry,  # jE jsonEntry
                                            headers=headers,
                                            rM=result_json["message"],  # rM resultMessage
                                            f="\t\t\t")
else:
    microtest["message"] = "Ответ не соответствует документации.\n{f} Входные параметры (headers): \n{f}{headers}\n\n" \
                           "{f} Выходные параметры: \n{f}{rM}".format(
                                                                    headers=headers,
                                                                    rM=result_json["message"],  # rM resultMessage
                                                                    f="\t\t\t")

data.append(microtest)

print_format(num_test, name_test, status_all, data)

# test 4. получение списка факультетов
name_test = "Получение списка факультетов"
num_test = 4
status_all = True
data = []
# test 4.A
params = {"id_university": 1}
headers = {}

microtest = struct_microtest("A", "Успешное получение списка факультетов")

result, microtest["time_work"] = test_get_method_with_params(url_faculty, headers, params)
result_json = result.json()

# print(result)
if result_json["message"]["code"] == 5:
    pass
else:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры : \n{f} {params} \n\n{f} Выходные параметры:\n{f} {rM}".format(
                                            params=params,
                                            rM=result_json["message"],  # rM resultMessage
                                            f="\t\t\t")

data.append(microtest)

# test 4.B
headers = {}
params = {"id_university": "o"}

microtest = struct_microtest("B", "айди университета - строка (неизвестная ошибка)")

result, microtest["time_work"] = test_get_method_with_params(url_faculty, headers, params)
result_json = result.json()

# print(result)
if result_json["message"]["code"] == -31:
    pass
elif result_json["message"]["code"] == 5:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры : \n{f} {params} \n\n{f} Выходные параметры:\n{f} {rM}".format(
                                            params=params,
                                            rM=result_json["message"],  # rM resultMessage
                                            f="\t\t\t")
else:
    microtest["message"] = "Ответ не соответствует документации.\n{f} Входные параметры: \n{f}{params} \n\n {f} " \
                           "Выходные параметры: \n{f}{rM}".format(
                                                                params=params,
                                                                rM=result_json["message"],  # rM resultMessage
                                                                f="\t\t\t")

data.append(microtest)

print_format(num_test, name_test, status_all, data)
"""
"""
# test 5. получение списка факультетов
name_test = "Получение списка предметов пользователя"
num_test = 5
status_all = True
data = []
# test 5.A
params = {"type_work": "course_work"}
headers = {"Token": token}

microtest = struct_microtest("A", "Успешное получение списка предметов пользователя")

result, microtest["time_work"] = test_get_method(url_faculty_course_subject, headers, params)
result_json = result.json()

# print(result)
if result_json["message"]["code"] == 7:
    pass
else:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {params} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
                                                    params=params,
                                                    headers=headers,
                                                    rM=result_json["message"],  # rM resultMessage
                                                    f="\t\t\t")

data.append(microtest)

# test 5.B
params = {"type_work": "course_work"}
headers = {"Token": "fdfs"}

microtest = struct_microtest("B", "Невалидный токен")

result, microtest["time_work"] = test_get_method_with_params(url_faculty_course_subject, headers, params)
result_json = result.json()

# print(result)
if result_json["message"]["code"] == -32:
    pass
elif result_json["message"]["code"] == 7:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {params} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
                                                    params=params,
                                                    headers=headers,
                                                    rM=result_json["message"],  # rM resultMessage
                                                    f="\t\t\t")
else:
    microtest["message"] = "Ответ не соответствует документации.\n{f} Входные параметры:\n{f}{params}\n{f} (headers):" \
                           " \n{f} {headers} \n\n {f} Выходные параметры: \n{f}{rM}".format(
                                                                        params=params,
                                                                        headers=headers,
                                                                        rM=result_json["message"],  # rM resultMessage
                                                                        f="\t\t\t")

data.append(microtest)

# test 5.C
params = {}
headers = {"Token": token}

microtest = struct_microtest("C", "Недостаточно аргументов для выполнения запроса")

result, microtest["time_work"] = test_get_method_with_params(url_faculty_course_subject, headers, params)
result_json = result.json()

# print(result)
if result_json["message"]["code"] == -33:
    pass
elif result_json["message"]["code"] == 7:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {params} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
                                                    params=params,
                                                    headers=headers,
                                                    rM=result_json["message"],  # rM resultMessage
                                                    f="\t\t\t")
else:
    microtest["message"] = "Ответ не соответствует документации.\n{f} Входные параметры:\n{f}{params}\n{f} (headers):" \
                           " \n{f} {headers} \n\n {f} Выходные параметры: \n{f}{rM}".format(
                                                                        params=params,
                                                                        headers=headers,
                                                                        rM=result_json["message"],  # rM resultMessage
                                                                        f="\t\t\t")

data.append(microtest)

# test 5.D
params = {"type_work": "sssfsdfs"}
headers = {"Token": token}

microtest = struct_microtest("D", "Неверный тип работы")

result, microtest["time_work"] = test_get_method_with_params(url_faculty_course_subject, headers, params)
result_json = result.json()

if result_json["message"]["code"] == -10:
    pass
elif result_json["message"]["code"] == 7:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {params} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
                                                    params=params,
                                                    headers=headers,
                                                    rM=result_json["message"],  # rM resultMessage
                                                    f="\t\t\t")
else:
    microtest["message"] = "Ответ не соответствует документации.\n{f} Входные параметры:\n{f}{params}\n{f} (headers):" \
                           " \n{f} {headers} \n\n {f} Выходные параметры: \n{f}{rM}".format(
                                                                        params=params,
                                                                        headers=headers,
                                                                        rM=result_json["message"],  # rM resultMessage
                                                                        f="\t\t\t")

data.append(microtest)

print_format(num_test, name_test, status_all, data)
"""
"""
# test 6. Получение семинаров для курса пользователя
name_test = "Получение семинаров для курса пользователя"
num_test = 6
status_all = True
data = []
# test 6.A
params = {"id_subject": 38}
headers = {"Token": token}

microtest = struct_microtest("A", "Успешное получение семинаров")

result, microtest["time_work"] = test_get_method_with_params(url_subject_seminar, headers, params)
result_json = result.json()

if result_json["message"]["code"] == 8:
    pass
else:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {params} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
                                                    params=params,
                                                    headers=headers,
                                                    rM=result_json["message"],  # rM resultMessage
                                                    f="\t\t\t")

data.append(microtest)

# test 6.B
params = {"id_subject": 54}
headers = {"Token": token}

microtest = struct_microtest("B", "Предмет, для которого нет семинара")

result, microtest["time_work"] = test_get_method_with_params(url_subject_seminar, headers, params)
result_json = result.json()

if result_json["message"]["code"] == -12:
    pass
elif result_json["message"]["code"] == 8:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {params} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
                                                    params=params,
                                                    headers=headers,
                                                    rM=result_json["message"],  # rM resultMessage
                                                    f="\t\t\t")
else:
    microtest["message"] = "Ответ не соответствует документации.\n{f} Входные параметры:\n{f}{params}\n{f} (headers):" \
                           " \n{f} {headers} \n\n {f} Выходные параметры: \n{f}{rM}".format(
                                                                        params=params,
                                                                        headers=headers,
                                                                        rM=result_json["message"],  # rM resultMessage
                                                                        f="\t\t\t")

data.append(microtest)

# test 6.C
params = {"id_subject": 34}
headers = {"Token": "efsefs"}

microtest = struct_microtest("C", "Невалидный токен")

result, microtest["time_work"] = test_get_method_with_params(url_subject_seminar, headers, params)
result_json = result.json()

if result_json["message"]["code"] == -32:
    pass
elif result_json["message"]["code"] == 8:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {params} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
                                                    params=params,
                                                    headers=headers,
                                                    rM=result_json["message"],  # rM resultMessage
                                                    f="\t\t\t")
else:
    microtest["message"] = "Ответ не соответствует документации.\n{f} Входные параметры:\n{f}{params}\n{f} (headers):" \
                           " \n{f} {headers} \n\n {f} Выходные параметры: \n{f}{rM}".format(
                                                                        params=params,
                                                                        headers=headers,
                                                                        rM=result_json["message"],  # rM resultMessage
                                                                        f="\t\t\t")

data.append(microtest)

# test 6.D
params = {}
headers = {"Token": token}

microtest = struct_microtest("D", "Недостаточно аргументов для выполнения запроса")

result, microtest["time_work"] = test_get_method_with_params(url_subject_seminar, headers, params)
result_json = result.json()

if result_json["message"]["code"] == -33:
    pass
elif result_json["message"]["code"] == 8:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {params} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
                                                    params=params,
                                                    headers=headers,
                                                    rM=result_json["message"],  # rM resultMessage
                                                    f="\t\t\t")
else:
    microtest["message"] = "Ответ не соответствует документации.\n{f} Входные параметры:\n{f}{params}\n{f} (headers):" \
                           " \n{f} {headers} \n\n {f} Выходные параметры: \n{f}{rM}".format(
                                                                        params=params,
                                                                        headers=headers,
                                                                        rM=result_json["message"],  # rM resultMessage
                                                                        f="\t\t\t")

data.append(microtest)

print_format(num_test, name_test, status_all, data)
"""
"""
# test 7. Получение вопросов семинара
name_test = "Получение вопросов семинара"
num_test = 7
status_all = True
data = []
# test 7.A
params = {"id_seminar": 53}
headers = {"Token": token}

microtest = struct_microtest("A", "Успешное получение вопросов")

result, microtest["time_work"] = test_get_method_with_params(url_subject_seminar_questions, headers, params)
result_json = result.json()

if result_json["message"]["code"] == 9:
    pass
else:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {params} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
                                                    params=params,
                                                    headers=headers,
                                                    rM=result_json["message"],  # rM resultMessage
                                                    f="\t\t\t")

data.append(microtest)

# test 7.B
params = {"id_seminar": 32}
headers = {"Token": token}

microtest = struct_microtest("B", "Нет вопросов для выбранного семинара")

result, microtest["time_work"] = test_get_method_with_params(url_subject_seminar_questions, headers, params)
result_json = result.json()

if result_json["message"]["code"] == -13:
    pass
elif result_json["message"]["code"] == 9:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {params} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
                                                    params=params,
                                                    headers=headers,
                                                    rM=result_json["message"],  # rM resultMessage
                                                    f="\t\t\t")
else:
    microtest["message"] = "Ответ не соответствует документации.\n{f} Входные параметры:\n{f}{params}\n{f} (headers):" \
                           " \n{f} {headers} \n\n {f} Выходные параметры: \n{f}{rM}".format(
                                                                        params=params,
                                                                        headers=headers,
                                                                        rM=result_json["message"],  # rM resultMessage
                                                                        f="\t\t\t")

data.append(microtest)

# test 7.C
params = {"id_seminar": 32}
headers = {"Token": "dfsdfsdf"}

microtest = struct_microtest("C", "Невалидный токен")

result, microtest["time_work"] = test_get_method_with_params(url_subject_seminar_questions, headers, params)
result_json = result.json()

if result_json["message"]["code"] == -32:
    pass
elif result_json["message"]["code"] == 9:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {params} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
                                                    params=params,
                                                    headers=headers,
                                                    rM=result_json["message"],  # rM resultMessage
                                                    f="\t\t\t")
else:
    microtest["message"] = "Ответ не соответствует документации.\n{f} Входные параметры:\n{f}{params}\n{f} (headers):" \
                           " \n{f} {headers} \n\n {f} Выходные параметры: \n{f}{rM}".format(
                                                                        params=params,
                                                                        headers=headers,
                                                                        rM=result_json["message"],  # rM resultMessage
                                                                        f="\t\t\t")

data.append(microtest)

# test 7.D
params = {}
headers = {"Token": token}

microtest = struct_microtest("D", "Недостаточно аргументов для выполнения запроса")

result, microtest["time_work"] = test_get_method_with_params(url_subject_seminar_questions, headers, params)
result_json = result.json()

if result_json["message"]["code"] == -33:
    pass
elif result_json["message"]["code"] == 9:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {params} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
                                                    params=params,
                                                    headers=headers,
                                                    rM=result_json["message"],  # rM resultMessage
                                                    f="\t\t\t")
else:
    microtest["message"] = "Ответ не соответствует документации.\n{f} Входные параметры:\n{f}{params}\n{f} (headers):" \
                           " \n{f} {headers} \n\n {f} Выходные параметры: \n{f}{rM}".format(
                                                                        params=params,
                                                                        headers=headers,
                                                                        rM=result_json["message"],  # rM resultMessage
                                                                        f="\t\t\t")

data.append(microtest)

print_format(num_test, name_test, status_all, data)
"""
"""
# test 8. Получение вопроса семинара
name_test = "Получение вопроса семинара"
num_test = 8
status_all = True
data = []
# test 8.A
params = {"id_question": 95}
headers = {"Token": token}

microtest = struct_microtest("A", "Успешное получение вопроса")

result, microtest["time_work"] = test_get_method_with_params(url_subject_seminar_question, headers, params)
result_json = result.json()

if result_json["message"]["code"] == 10:
    pass
else:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {params} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
                                                    params=params,
                                                    headers=headers,
                                                    rM=result_json["message"],  # rM resultMessage
                                                    f="\t\t\t")

data.append(microtest)

# test 8.B
params = {"id_question": 2}
headers = {"Token": token}

microtest = struct_microtest("B", "Нет вопросов для выбранного семинара")

result, microtest["time_work"] = test_get_method_with_params(url_subject_seminar_question, headers, params)
result_json = result.json()

if result_json["message"]["code"] == -14:
    pass
elif result_json["message"]["code"] == 10:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {params} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
                                                    params=params,
                                                    headers=headers,
                                                    rM=result_json["message"],  # rM resultMessage
                                                    f="\t\t\t")
else:
    microtest["message"] = "Ответ не соответствует документации.\n{f} Входные параметры:\n{f}{params}\n{f} (headers):" \
                           " \n{f} {headers} \n\n {f} Выходные параметры: \n{f}{rM}".format(
                                                                        params=params,
                                                                        headers=headers,
                                                                        rM=result_json["message"],  # rM resultMessage
                                                                        f="\t\t\t")

data.append(microtest)

# test 8.C
params = {"id_question": 2}
headers = {"Token": "dfsdfsdf"}

microtest = struct_microtest("C", "Невалидный токен")

result, microtest["time_work"] = test_get_method_with_params(url_subject_seminar_question, headers, params)
result_json = result.json()

if result_json["message"]["code"] == -32:
    pass
elif result_json["message"]["code"] == 10:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {params} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
                                                    params=params,
                                                    headers=headers,
                                                    rM=result_json["message"],  # rM resultMessage
                                                    f="\t\t\t")
else:
    microtest["message"] = "Ответ не соответствует документации.\n{f} Входные параметры:\n{f}{params}\n{f} (headers):" \
                           " \n{f} {headers} \n\n {f} Выходные параметры: \n{f}{rM}".format(
                                                                        params=params,
                                                                        headers=headers,
                                                                        rM=result_json["message"],  # rM resultMessage
                                                                        f="\t\t\t")

data.append(microtest)

# test 8.D
params = {}
headers = {"Token": token}

microtest = struct_microtest("D", "Недостаточно аргументов для выполнения запроса")

result, microtest["time_work"] = test_get_method_with_params(url_subject_seminar_question, headers, params)
result_json = result.json()

if result_json["message"]["code"] == -33:
    pass
elif result_json["message"]["code"] == 10:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {params} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
                                                    params=params,
                                                    headers=headers,
                                                    rM=result_json["message"],  # rM resultMessage
                                                    f="\t\t\t")
else:
    microtest["message"] = "Ответ не соответствует документации.\n{f} Входные параметры:\n{f}{params}\n{f} (headers):" \
                           " \n{f} {headers} \n\n {f} Выходные параметры: \n{f}{rM}".format(
                                                                        params=params,
                                                                        headers=headers,
                                                                        rM=result_json["message"],  # rM resultMessage
                                                                        f="\t\t\t")

data.append(microtest)

print_format(num_test, name_test, status_all, data)
"""
"""
# test 9. Получение вопроса экзамена
name_test = "Получение вопросов экзамена"
num_test = 9
status_all = True
data = []
# test 9.A
params = {"id_subject": 34}
headers = {"Token": token}

microtest = struct_microtest("A", "Успешное получение вопросов")

result, microtest["time_work"] = test_get_method_with_params(url_subject_exams, headers, params)
result_json = result.json()

if result_json["message"]["code"] == 11:
    pass
else:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {params} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
                                                    params=params,
                                                    headers=headers,
                                                    rM=result_json["message"],  # rM resultMessage
                                                    f="\t\t\t")

data.append(microtest)

# test 9.B
params = {"id_subject": 2}
headers = {"Token": token}

microtest = struct_microtest("B", "Нет вопросов для выбранного экзамена")

result, microtest["time_work"] = test_get_method_with_params(url_subject_exams, headers, params)
result_json = result.json()

if result_json["message"]["code"] == -415:
    pass
elif result_json["message"]["code"] == 11:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {params} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
                                                    params=params,
                                                    headers=headers,
                                                    rM=result_json["message"],  # rM resultMessage
                                                    f="\t\t\t")
else:
    microtest["message"] = "Ответ не соответствует документации.\n{f} Входные параметры:\n{f}{params}\n{f} (headers):" \
                           " \n{f} {headers} \n\n {f} Выходные параметры: \n{f}{rM}".format(
                                                                        params=params,
                                                                        headers=headers,
                                                                        rM=result_json["message"],  # rM resultMessage
                                                                        f="\t\t\t")

data.append(microtest)

# test 9.C
params = {"id_subject": 34}
headers = {"Token": "dfsdfsdf"}

microtest = struct_microtest("C", "Невалидный токен")

result, microtest["time_work"] = test_get_method_with_params(url_subject_exams, headers, params)
result_json = result.json()

if result_json["message"]["code"] == -32:
    pass
elif result_json["message"]["code"] == 11:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {params} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
                                                    params=params,
                                                    headers=headers,
                                                    rM=result_json["message"],  # rM resultMessage
                                                    f="\t\t\t")
else:
    microtest["message"] = "Ответ не соответствует документации.\n{f} Входные параметры:\n{f}{params}\n{f} (headers):" \
                           " \n{f} {headers} \n\n {f} Выходные параметры: \n{f}{rM}".format(
                                                                        params=params,
                                                                        headers=headers,
                                                                        rM=result_json["message"],  # rM resultMessage
                                                                        f="\t\t\t")

data.append(microtest)

# test 9.D
params = {}
headers = {"Token": token}

microtest = struct_microtest("D", "Недостаточно аргументов для выполнения запроса")

result, microtest["time_work"] = test_get_method_with_params(url_subject_exams, headers, params)
result_json = result.json()

if result_json["message"]["code"] == -33:
    pass
elif result_json["message"]["code"] == 11:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {params} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
                                                    params=params,
                                                    headers=headers,
                                                    rM=result_json["message"],  # rM resultMessage
                                                    f="\t\t\t")
else:
    microtest["message"] = "Ответ не соответствует документации.\n{f} Входные параметры:\n{f}{params}\n{f} (headers):" \
                           " \n{f} {headers} \n\n {f} Выходные параметры: \n{f}{rM}".format(
                                                                        params=params,
                                                                        headers=headers,
                                                                        rM=result_json["message"],  # rM resultMessage
                                                                        f="\t\t\t")

data.append(microtest)

print_format(num_test, name_test, status_all, data)
"""
"""
# test 10. Получение вопроса экзамена
name_test = "Получение вопроса экзамена"
num_test = 10
status_all = True
data = []
# test 10.A
params = {"id_question_exam": 40}
headers = {"Token": token}

microtest = struct_microtest("A", "Успешное получение вопроса")

result, microtest["time_work"] = test_get_method_with_params(url_subject_exam_question, headers, params)
result_json = result.json()

if result_json["message"]["code"] == 12:
    pass
else:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {params} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
                                                    params=params,
                                                    headers=headers,
                                                    rM=result_json["message"],  # rM resultMessage
                                                    f="\t\t\t")

data.append(microtest)

# test 10.B
params = {"id_question_exam": 2}
headers = {"Token": token}

microtest = struct_microtest("B", "Нет вопросов для выбранного экзамена")

result, microtest["time_work"] = test_get_method_with_params(url_subject_exam_question, headers, params)
result_json = result.json()

if result_json["message"]["code"] == -16:
    pass
elif result_json["message"]["code"] == 12:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {params} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
                                                    params=params,
                                                    headers=headers,
                                                    rM=result_json["message"],  # rM resultMessage
                                                    f="\t\t\t")
else:
    microtest["message"] = "Ответ не соответствует документации.\n{f} Входные параметры:\n{f}{params}\n{f} (headers):" \
                           " \n{f} {headers} \n\n {f} Выходные параметры: \n{f}{rM}".format(
                                                                        params=params,
                                                                        headers=headers,
                                                                        rM=result_json["message"],  # rM resultMessage
                                                                        f="\t\t\t")

data.append(microtest)

# test 10.C
params = {"id_question_exam": 40}
headers = {"Token": "dfsdfsdf"}

microtest = struct_microtest("C", "Невалидный токен")

result, microtest["time_work"] = test_get_method_with_params(url_subject_exam_question, headers, params)
result_json = result.json()

if result_json["message"]["code"] == -32:
    pass
elif result_json["message"]["code"] == 12:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {params} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
                                                    params=params,
                                                    headers=headers,
                                                    rM=result_json["message"],  # rM resultMessage
                                                    f="\t\t\t")
else:
    microtest["message"] = "Ответ не соответствует документации.\n{f} Входные параметры:\n{f}{params}\n{f} (headers):" \
                           " \n{f} {headers} \n\n {f} Выходные параметры: \n{f}{rM}".format(
                                                                        params=params,
                                                                        headers=headers,
                                                                        rM=result_json["message"],  # rM resultMessage
                                                                        f="\t\t\t")

data.append(microtest)

# test 10.D
params = {}
headers = {"Token": token}

microtest = struct_microtest("D", "Недостаточно аргументов для выполнения запроса")

result, microtest["time_work"] = test_get_method_with_params(url_subject_exam_question, headers, params)
result_json = result.json()

if result_json["message"]["code"] == -35:
    pass
elif result_json["message"]["code"] == 12:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {params} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
                                                    params=params,
                                                    headers=headers,
                                                    rM=result_json["message"],  # rM resultMessage
                                                    f="\t\t\t")
else:
    microtest["message"] = "Ответ не соответствует документации.\n{f} Входные параметры:\n{f}{params}\n{f} (headers):" \
                           " \n{f} {headers} \n\n {f} Выходные параметры: \n{f}{rM}".format(
                                                                        params=params,
                                                                        headers=headers,
                                                                        rM=result_json["message"],  # rM resultMessage
                                                                        f="\t\t\t")

data.append(microtest)

print_format(num_test, name_test, status_all, data)
"""
"""
# test 11. Получение списка курсовых
name_test = "Получение списка курсовых"
num_test = 11
status_all = True
data = []
# test 11.A
params = {"id_subject": 34}
headers = {"Token": token}

microtest = struct_microtest("A", "Успешное получение курсовых")

result, microtest["time_work"] = test_get_method_with_params(url_subject_course_works, headers, params)
result_json = result.json()

if result_json["message"]["code"] == 13:
    pass
else:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {params} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
                                                    params=params,
                                                    headers=headers,
                                                    rM=result_json["message"],  # rM resultMessage
                                                    f="\t\t\t")

data.append(microtest)

# test 11.B
params = {"id_subject": 34}
headers = {"Token": "dfsdfsdf"}

microtest = struct_microtest("B", "Невалидный токен")

result, microtest["time_work"] = test_get_method_with_params(url_subject_course_works, headers, params)
result_json = result.json()

if result_json["message"]["code"] == 436:
    pass
elif result_json["message"]["code"] == 13:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {params} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
                                                    params=params,
                                                    headers=headers,
                                                    rM=result_json["message"],  # rM resultMessage
                                                    f="\t\t\t")
else:
    microtest["message"] = "Ответ не соответствует документации.\n{f} Входные параметры:\n{f}{params}\n{f} (headers):" \
                           " \n{f} {headers} \n\n {f} Выходные параметры: \n{f}{rM}".format(
                                                                        params=params,
                                                                        headers=headers,
                                                                        rM=result_json["message"],  # rM resultMessage
                                                                        f="\t\t\t")

data.append(microtest)

# test 11.C
params = {}
headers = {"Token": token}

microtest = struct_microtest("C", "Недостаточно аргументов для выполнения запроса")

result, microtest["time_work"] = test_get_method_with_params(url_subject_course_works, headers, params)
result_json = result.json()

if result_json["message"]["code"] == 437:
    pass
elif result_json["message"]["code"] == 13:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {params} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
                                                    params=params,
                                                    headers=headers,
                                                    rM=result_json["message"],  # rM resultMessage
                                                    f="\t\t\t")
else:
    microtest["message"] = "Ответ не соответствует документации.\n{f} Входные параметры:\n{f}{params}\n{f} (headers):" \
                           " \n{f} {headers} \n\n {f} Выходные параметры: \n{f}{rM}".format(
                                                                        params=params,
                                                                        headers=headers,
                                                                        rM=result_json["message"],  # rM resultMessage
                                                                        f="\t\t\t")

data.append(microtest)

print_format(num_test, name_test, status_all, data)
"""
"""
# test 12. Оформление заказа курсовой
name_test = "Оформление заказа курсовой"
num_test = 12
status_all = True
data = []
# test 12.A
json_entry = {
          "id_theme": 58,
          "email": "de@gmail.com",
          "phone": "string",
          "name": "deniska",
          "text": "string"
        }
headers = {"Token": token}

microtest = struct_microtest("A", "Успешное оформление заказа")

result, microtest["time_work"] = test_post_method(url_subject_course_work_order, json_entry, headers)
result_json = result.json()

if result_json["message"]["code"] == 14:
    pass
else:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {jE} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
                                                    jE=json_entry,  # jE json_entry
                                                    headers=headers,
                                                    rM=result_json["message"],  # rM resultMessage
                                                    f="\t\t\t")

data.append(microtest)

# test 12.B
json_entry = {
          "id_theme": 0,
          "email": "string",
          "phone": "string",
          "name": "string",
          "text": "string"
        }
headers = {"Token": "dawfaf"}

microtest = struct_microtest("B", "Невалидный токен")

result, microtest["time_work"] = test_post_method(url_subject_course_work_order, json_entry, headers)
result_json = result.json()

if result_json["message"]["code"] == -32:
    pass
elif result_json["message"]["code"] == 14:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {jE} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
                                                    jE=json_entry,  # jE json_entry
                                                    headers=headers,
                                                    rM=result_json["message"],  # rM resultMessage
                                                    f="\t\t\t")
else:
    microtest["message"] = "Ответ не соответствует документации.\n{f} Входные параметры:\n{f}{jE}\n{f} (headers):" \
                           " \n{f} {headers} \n\n {f} Выходные параметры: \n{f}{rM}".format(
                                                                        jE=json_entry,  # jE json_entry
                                                                        headers=headers,
                                                                        rM=result_json["message"],  # rM resultMessage
                                                                        f="\t\t\t")

data.append(microtest)

# test 12.C
json_entry = {
    "email": "string",
    "phone": "string",
    "name": "string",
    "text": "string"
}
headers = {"Token": token}

microtest = struct_microtest("C", "Недостаточно аргументов для выполнения запроса")

result, microtest["time_work"] = test_post_method(url_subject_course_work_order, json_entry, headers)
result_json = result.json()

if result_json["message"]["code"] == -33:
    pass
elif result_json["message"]["code"] == 14:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {jE} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
        jE=json_entry,  # jE json_entry
        headers=headers,
        rM=result_json["message"],  # rM resultMessage
        f="\t\t\t")
else:
    microtest["message"] = "Ответ не соответствует документации.\n{f} Входные параметры:\n{f}{jE}\n{f} (headers):" \
                           " \n{f} {headers} \n\n {f} Выходные параметры: \n{f}{rM}".format(
        jE=json_entry,  # jE json_entry
        headers=headers,
        rM=result_json["message"],  # rM resultMessage
        f="\t\t\t")

data.append(microtest)

print_format(num_test, name_test, status_all, data)
"""
"""
# test 13. Получение избранного
name_test = "Получение списка избранного"
num_test = 13
status_all = True
data = []
# test 13.A
params = {"is_seminar_question": True}
headers = {"Token": token}

microtest = struct_microtest("A", "Успешное получение избранных вопросов")

result, microtest["time_work"] = test_get_method_with_params(url_favorites, headers, params)
result_json = result.json()

if result_json["message"]["code"] == 17:
    pass
else:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {params} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
                                                    params=params,
                                                    headers=headers,
                                                    rM=result_json["message"],  # rM resultMessage
                                                    f="\t\t\t")

data.append(microtest)

# test 13.B
params = {"is_seminar_question": True}
headers = {"Token": "dfsdfsdf"}

microtest = struct_microtest("B", "Невалидный токен")

result, microtest["time_work"] = test_get_method_with_params(url_favorites, headers, params)
result_json = result.json()

if result_json["message"]["code"] == -32:
    pass
elif result_json["message"]["code"] == 17:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {params} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
                                                    params=params,
                                                    headers=headers,
                                                    rM=result_json["message"],  # rM resultMessage
                                                    f="\t\t\t")
else:
    microtest["message"] = "Ответ не соответствует документации.\n{f} Входные параметры:\n{f}{params}\n{f} (headers):" \
                           " \n{f} {headers} \n\n {f} Выходные параметры: \n{f}{rM}".format(
                                                                        params=params,
                                                                        headers=headers,
                                                                        rM=result_json["message"],  # rM resultMessage
                                                                        f="\t\t\t")

data.append(microtest)

# test 13.C
params = {"is_seminar_question": True}
headers = {}

microtest = struct_microtest("C", "Недостаточно аргументов для выполнения запроса")

result, microtest["time_work"] = test_get_method_with_params(url_favorites, headers, params)
result_json = result.json()

if result_json["message"]["code"] == -33:
    pass
elif result_json["message"]["code"] == 17:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {params} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
                                                    params=params,
                                                    headers=headers,
                                                    rM=result_json["message"],  # rM resultMessage
                                                    f="\t\t\t")
else:
    microtest["message"] = "Ответ не соответствует документации.\n{f} Входные параметры:\n{f}{params}\n{f} (headers):" \
                           " \n{f} {headers} \n\n {f} Выходные параметры: \n{f}{rM}".format(
                                                                        params=params,
                                                                        headers=headers,
                                                                        rM=result_json["message"],  # rM resultMessage
                                                                        f="\t\t\t")

data.append(microtest)

print_format(num_test, name_test, status_all, data)
"""
"""
# test 14. Добавление в избранное
name_test = "Добавление в избранное"
num_test = 14
status_all = True
data = []
# test 14.A
json_entry = {
              "is_seminar_question": True,
              "id_question": 134
            }
headers = {"Token": token}

microtest = struct_microtest("A", "Успешное добавление вопроса в избранное")

result, microtest["time_work"] = test_post_method(url_favorite_add, json_entry, headers)
result_json = result.json()

if result_json["message"]["code"] == 15:
    pass
else:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {jE} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
                                                    jE=json_entry,  # jE json_entry
                                                    headers=headers,
                                                    rM=result_json["message"],  # rM resultMessage
                                                    f="\t\t\t")

data.append(microtest)

# test 14.B
json_entry = {
              "is_seminar_question": False,
              "id_question": 40
            }
headers = {"Token": "dawfaf"}

microtest = struct_microtest("B", "Невалидный токен")

result, microtest["time_work"] = test_post_method(url_favorite_add, json_entry, headers)
result_json = result.json()

if result_json["message"]["code"] == -32:
    pass
elif result_json["message"]["code"] == 15:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {jE} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
                                                    jE=json_entry,  # jE json_entry
                                                    headers=headers,
                                                    rM=result_json["message"],  # rM resultMessage
                                                    f="\t\t\t")
else:
    microtest["message"] = "Ответ не соответствует документации.\n{f} Входные параметры:\n{f}{jE}\n{f} (headers):" \
                           " \n{f} {headers} \n\n {f} Выходные параметры: \n{f}{rM}".format(
                                                                        jE=json_entry,  # jE json_entry
                                                                        headers=headers,
                                                                        rM=result_json["message"],  # rM resultMessage
                                                                        f="\t\t\t")

data.append(microtest)

# test 14.C
json_entry = {
              "id_question": 40
            }
headers = {"Token": token}

microtest = struct_microtest("C", "Недостаточно аргументов для выполнения запроса")

result, microtest["time_work"] = test_post_method(url_favorite_add, json_entry, headers)
result_json = result.json()

if result_json["message"]["code"] == -33:
    pass
elif result_json["message"]["code"] == 15:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {jE} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
        jE=json_entry,  # jE json_entry
        headers=headers,
        rM=result_json["message"],  # rM resultMessage
        f="\t\t\t")
else:
    microtest["message"] = "Ответ не соответствует документации.\n{f} Входные параметры:\n{f}{jE}\n{f} (headers):" \
                           " \n{f} {headers} \n\n {f} Выходные параметры: \n{f}{rM}".format(
        jE=json_entry,  # jE json_entry
        headers=headers,
        rM=result_json["message"],  # rM resultMessage
        f="\t\t\t")

data.append(microtest)

print_format(num_test, name_test, status_all, data)
"""
"""
# test 15. Получение вопросов по списку id
name_test = "Получение вопросов по списку id"
num_test = 15
status_all = True
data = []
# test 15.A
json_entry = {
          "is_seminar_question": True,
          "list_questions": [
            134
          ]
        }
headers = {"Token": token}

microtest = struct_microtest("A", "Успешное получение вопросов по списку id")

result, microtest["time_work"] = test_post_method(url_favorite_questions, json_entry, headers)
result_json = result.json()

if result_json["message"]["code"] == 18:
    pass
else:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {jE} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
                                                    jE=json_entry,  # jE json_entry
                                                    headers=headers,
                                                    rM=result_json["message"],  # rM resultMessage
                                                    f="\t\t\t")

data.append(microtest)

# test 15.B
json_entry = {
          "is_seminar_question": False,
          "list_questions": [
            40
          ]
        }
headers = {"Token": "dawfaf"}

microtest = struct_microtest("B", "Невалидный токен")

result, microtest["time_work"] = test_post_method(url_favorite_questions, json_entry, headers)
result_json = result.json()

if result_json["message"]["code"] == -32:
    pass
elif result_json["message"]["code"] == 18:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {jE} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
                                                    jE=json_entry,  # jE json_entry
                                                    headers=headers,
                                                    rM=result_json["message"],  # rM resultMessage
                                                    f="\t\t\t")
else:
    microtest["message"] = "Ответ не соответствует документации.\n{f} Входные параметры:\n{f}{jE}\n{f} (headers):" \
                           " \n{f} {headers} \n\n {f} Выходные параметры: \n{f}{rM}".format(
                                                                        jE=json_entry,  # jE json_entry
                                                                        headers=headers,
                                                                        rM=result_json["message"],  # rM resultMessage
                                                                        f="\t\t\t")

data.append(microtest)

# test 15.C
json_entry = {
          "is_seminar_question": False
        }
headers = {"Token": token}

microtest = struct_microtest("C", "Недостаточно аргументов для выполнения запроса")

result, microtest["time_work"] = test_post_method(url_favorite_questions, json_entry, headers)
result_json = result.json()

if result_json["message"]["code"] == -33:
    pass
elif result_json["message"]["code"] == 18:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {jE} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
        jE=json_entry,  # jE json_entry
        headers=headers,
        rM=result_json["message"],  # rM resultMessage
        f="\t\t\t")
else:
    microtest["message"] = "Ответ не соответствует документации.\n{f} Входные параметры:\n{f}{jE}\n{f} (headers):" \
                           " \n{f} {headers} \n\n {f} Выходные параметры: \n{f}{rM}".format(
        jE=json_entry,  # jE json_entry
        headers=headers,
        rM=result_json["message"],  # rM resultMessage
        f="\t\t\t")

data.append(microtest)

# test 15.D
json_entry = {
        "is_seminar_question": False,
        "list_questions": [
            67567567
        ]
    }
headers = {"Token": token}

microtest = struct_microtest("D", "Список доступных вопросов пуст")

result, microtest["time_work"] = test_post_method(url_favorite_questions, json_entry, headers)
result_json = result.json()

if result_json["message"]["code"] == -21:
    pass
elif result_json["message"]["code"] == 18:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {jE} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
        jE=json_entry,  # jE json_entry
        headers=headers,
        rM=result_json["message"],  # rM resultMessage
        f="\t\t\t")
else:
    microtest["message"] = "Ответ не соответствует документации.\n{f} Входные параметры:\n{f}{jE}\n{f} (headers):" \
                           " \n{f} {headers} \n\n {f} Выходные параметры: \n{f}{rM}".format(
        jE=json_entry,  # jE json_entry
        headers=headers,
        rM=result_json["message"],  # rM resultMessage
        f="\t\t\t")

data.append(microtest)

print_format(num_test, name_test, status_all, data)
"""
"""
# test 16. Удаление указанного вопроса из избранного
name_test = "Удаление указанного вопроса из избранного"
num_test = 16
status_all = True
data = []
# test 16.A
json_entry = {
          "is_seminar_question": False,
          "list_questions": [
            40
          ]
        }
headers = {"Token": token}

microtest = struct_microtest("A", "Успешное удаление указанного вопроса из избранного")

result, microtest["time_work"] = test_post_method(url_favorite_remove, json_entry, headers)
result_json = result.json()

if result_json["message"]["code"] == 16:
    pass
else:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {jE} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
                                                    jE=json_entry,  # jE json_entry
                                                    headers=headers,
                                                    rM=result_json["message"],  # rM resultMessage
                                                    f="\t\t\t")

data.append(microtest)

# test 16.B
json_entry = {
          "is_seminar_question": False,
          "list_questions": [
            40
          ]
        }
headers = {"Token": "dawfaf"}

microtest = struct_microtest("B", "Невалидный токен")

result, microtest["time_work"] = test_post_method(url_favorite_remove, json_entry, headers)
result_json = result.json()

if result_json["message"]["code"] == -32:
    pass
elif result_json["message"]["code"] == 16:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {jE} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
                                                    jE=json_entry,  # jE json_entry
                                                    headers=headers,
                                                    rM=result_json["message"],  # rM resultMessage
                                                    f="\t\t\t")
else:
    microtest["message"] = "Ответ не соответствует документации.\n{f} Входные параметры:\n{f}{jE}\n{f} (headers):" \
                           " \n{f} {headers} \n\n {f} Выходные параметры: \n{f}{rM}".format(
                                                                        jE=json_entry,  # jE json_entry
                                                                        headers=headers,
                                                                        rM=result_json["message"],  # rM resultMessage
                                                                        f="\t\t\t")

data.append(microtest)

# test 16.C
json_entry = {
          "list_questions": [
            40
          ]
        }
headers = {"Token": token}

microtest = struct_microtest("C", "Недостаточно аргументов для выполнения запроса")

result, microtest["time_work"] = test_post_method(url_favorite_remove, json_entry, headers)
result_json = result.json()

if result_json["message"]["code"] == -33:
    pass
elif result_json["message"]["code"] == 16:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры:\n{f} {jE} \n{f} (headers): \n{f} {headers} \n\n{f} Выходные " \
                           "параметры:\n{f} {rM}".format(
        jE=json_entry,  # jE json_entry
        headers=headers,
        rM=result_json["message"],  # rM resultMessage
        f="\t\t\t")
else:
    microtest["message"] = "Ответ не соответствует документации.\n{f} Входные параметры:\n{f}{jE}\n{f} (headers):" \
                           " \n{f} {headers} \n\n {f} Выходные параметры: \n{f}{rM}".format(
        jE=json_entry,  # jE json_entry
        headers=headers,
        rM=result_json["message"],  # rM resultMessage
        f="\t\t\t")

data.append(microtest)

print_format(num_test, name_test, status_all, data)
"""
"""
# test 17. Удаление избранного
name_test = "Удаление всего избранного"
num_test = 17
status_all = True
data = []
# test 17.A
headers = {"Token": token}

microtest = struct_microtest("A", "Успешное удаление избранного")

result, microtest["time_work"] = test_get_method(url_favorite_drop, headers)
result_json = result.json()

if result_json["message"]["code"] == 16:
    pass
else:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры (headers): \n{f} {headers} \n\n{f} Выходные параметры:\n{f} {rM}".format(
                                            headers=headers,
                                            rM=result_json["message"],  # rM resultMessage
                                            f="\t\t\t")

data.append(microtest)

# test 17.B
headers = {}

microtest = struct_microtest("B", "Недостаточно аргументов для выполнения запроса")

result, microtest["time_work"] = test_get_method(url_user_logout, headers)
result_json = result.json()

if result_json["message"]["code"] == -33:
    pass
elif result_json["message"]["code"] == 16:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры (headers): \n{f} {headers} \n\n{f} Выходные параметры:\n{f} {rM}".format(
        headers=headers,
        rM=result_json["message"],  # rM resultMessage
        f="\t\t\t")
else:
    microtest["message"] = "Ответ не соответствует документации.\n{f} Входные параметры (headers): \n{f}{headers}\n\n" \
                           "{f} Выходные параметры: \n{f}{rM}".format(
                                                                    headers=headers,
                                                                    rM=result_json["message"],  # rM resultMessage
                                                                    f="\t\t\t")

data.append(microtest)

print_format(num_test, name_test, status_all, data)
"""
"""
# test 18. выход пользователя
name_test = "Проверка выхода"
num_test = 18
status_all = True
data = []
# test 18.A
headers = {"Token": token}

microtest = struct_microtest("A", "Успешный выход")

result, microtest["time_work"] = test_get_method(url_user_logout, headers)
result_json = result.json()

if result_json["message"]["code"] == 2:
    pass
else:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры (headers): \n{f} {headers} \n\n{f} Выходные параметры:\n{f} {rM}".format(
                                            headers=headers,
                                            rM=result_json["message"],  # rM resultMessage
                                            f="\t\t\t")

data.append(microtest)

# test 18.B
headers = {}

microtest = struct_microtest("B", "Недостаточно аргументов для выполнения запроса")

result, microtest["time_work"] = test_get_method(url_user_logout, headers)
result_json = result.json()

if result_json["message"]["code"] == -33:
    pass
elif result_json["message"]["code"] == 2:
    microtest["status"] = False
    status_all = False
    microtest["message"] = "Входные параметры (headers): \n{f} {headers} \n\n{f} Выходные параметры:\n{f} {rM}".format(
        headers=headers,
        rM=result_json["message"],  # rM resultMessage
        f="\t\t\t")
else:
    microtest["message"] = "Ответ не соответствует документации.\n{f} Входные параметры (headers): \n{f}{headers}\n\n" \
                           "{f} Выходные параметры: \n{f}{rM}".format(
                                                                    headers=headers,
                                                                    rM=result_json["message"],  # rM resultMessage
                                                                    f="\t\t\t")

data.append(microtest)

print_format(num_test, name_test, status_all, data)
"""
"""
test 1 <name test>  status T/F  time <time response>     а) t2-t1 б) time response
    A) <name microtest>    status T/F
    B) <name microtest>    status F
      <message —- data>


test 2 <name test>  status T/F  time <time response>
    A) <name microtest>    status T/F
    B) <name microtest>    status F
      <message —- data>
"""