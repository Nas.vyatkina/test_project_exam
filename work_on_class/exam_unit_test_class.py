import requests
import time
from termcolor import colored


class Token:
    token = None

    def get(self):
        return self.token


class baseWorkWithNetwork:
    def __init__(self, base_url):
        self.base_url = base_url
    # метод POST
    #@staticmethod
    def test_post_method(self,url_method, json_entry, headers):
        start_time = time.time()
        result = requests.post(url=self.base_url + url_method, json=json_entry, headers=headers)
        end_time = time.time()
        time_work = end_time - start_time
        return result.json(), time_work, result.headers

    # метод GET
    #@staticmethod
    def test_get_method(self, url_method, headers, params):
        start_time = time.time()
        result = requests.get(url=self.base_url + url_method, headers=headers, params=params)
        end_time = time.time()
        time_work = end_time - start_time
        return result.json(), time_work, result.headers


class baseMicrotest(baseWorkWithNetwork):
    """
        type    string  POST, GET,
    """
    microtest = {
        "index": None,
        "name": None,
        "status": True,
        "message": None,
        "time_work": None}

    return_headers = None
    f = "\t\t\t"
    __message = "Входные параметры:\n {f}{jE} \n{f}{p} \n{f} Headers:\n {f}{h} \n\n{f} Выходные параметры:\n {f}{rM}" \
                "\n\n {f}{mesResOut}"

    def req_func(self, dict_, result_json, keys):
        res = []
        for key in keys:
            if type(key) == str:
                if dict_.get(key, False):
                    if (type(result_json.get(key, False)) != dict_[key]["type"]) is True:
                        res.append(key)
                    else:
                        if dict_[key]["next"] is not None:
                            if type(result_json[key]) == list:
                                key_next = range(len(result_json[key]))
                            else:
                                key_next = result_json[key].keys()
                            tmp = self.req_func(dict_=dict_[key]["next"], result_json=result_json[key], keys=key_next)
                            if len(tmp) > 0:
                                res += tmp
                else:
                    res.append(key)
            else:
                try:
                    if ((type(result_json[key])) != dict_[0]["type"]) is True:
                        res.append(key)
                    else:
                        if dict_[0]["next"] is not None:
                            if type(result_json[key]) == list:
                                key_next = range(len(result_json[key]))
                            else:
                                key_next = result_json[key].keys()
                            tmp = self.req_func(dict_=dict_[0]["next"], result_json=result_json[key], keys=key_next)
                            if len(tmp) > 0:
                                res += tmp
                except IndexError:
                    res.append(key)
        return res

    def __init__(self, headers, struct_microtest, type, url, response_code, check_for_output, base_url,
                 json_entry=False, params=False):
        assert type in ["POST", "GET"]
        super().__init__(base_url=base_url)
        self.__clean()
        if headers.get("Token", False):
            try:
                headers["Token"] = headers["Token"]()
            except TypeError:
                pass
            except Exception as e:
                print(e)

        self.headers = headers

        self.microtest["index"] = struct_microtest["index"]
        self.microtest["name"] = struct_microtest["name"]
        self.type = type
        self.url = url
        if not json_entry:
            self.json_entry = ""
        else:
            self.json_entry = json_entry
        if not params:
            self.params = ""
        else:
            self.params = params
        self.__response_code = response_code
        self.check_for_output = check_for_output
        self.message_check_output = ""

    def __clean(self):
        self.microtest = {
            "index": None,
            "name": None,
            "status": True,
            "message": None,
            "time_work": None}
        self.return_headers = None

    def __action(self, action, result, message, mes_res_output):
        if action is None:
            pass
        elif action is True:
            self.microtest["message"] = message.format(
                jE=self.json_entry,
                p=self.params,
                h=self.headers,
                f=self.f,
                rM=result["message"],
                mesResOut=mes_res_output)
        elif action is False:
            self.microtest["message"] = message.format(
                jE=self.json_entry,
                p=self.params,
                f=self.f,
                h=self.headers,
                rM=result["message"],
                mesResOut=mes_res_output)
            self.microtest["status"] = False

    def start(self):
        if self.type == "POST":
            result, self.microtest["time_work"], self.return_headers = self.test_post_method(
                                                                                url_method=self.url,
                                                                                json_entry=self.json_entry,
                                                                                headers=self.headers)
        elif self.type == "GET":
            result, self.microtest["time_work"], self.return_headers = self.test_get_method(
                                                                                    url_method=self.url,
                                                                                    headers=self.headers,
                                                                                    params=self.params)

        # если есть структура для проверки получаемого json, то проверяем
        if self.check_for_output:
            keys = result.keys()
            self.message_check_output = self.req_func(dict_=self.check_for_output, result_json=result, keys=keys)

        resp_code = result["message"]["code"]
        if self.message_check_output:
            self.message_check_output = "Неверный тип получаемого ключа/ключ отсутствует " + str(self.message_check_output)
            self.__action(action=False,
                          result=result,
                          message=self.__message,
                          mes_res_output=self.message_check_output)
            # смотрим есть ли доп функция
            if self.__response_code[resp_code]["func"]:
                self.__response_code[resp_code]["func"](headers=self.return_headers)
        else:
            if self.__response_code.get(resp_code, False):
                action = self.__response_code.get(resp_code)
                self.__action(action=action["action"],
                              result=result,
                              message=self.__message,
                              mes_res_output=self.message_check_output)
                # смотрим есть ли доп функция
                if self.__response_code[resp_code]["func"]:
                    self.__response_code[resp_code]["func"](headers=self.return_headers)
            else:  # в том случае если код ответа не найден в нашей входной структуре
                action = self.__response_code.get("else")
                self.__action(action=action["action"],
                              result=result,
                              message="Ответ не соответствует документации.\n\t\t\t" + self.__message,
                              mes_res_output=self.message_check_output)


class baseTest():
    def __init__(self, name, num, microtest_data, base_url):
        assert type(num) == int
        assert type(name) == str
        assert type(base_url) == str
        self.name = name
        self.num = num
        self.status = True
        self.microtest_data = microtest_data
        self.data = []
        self.base_url = base_url

    def __print_format(self):
        if self.status:
            status_all = colored(self.status, "green")
        else:
            status_all = colored(self.status, "red")
        print("test {number}. {name}. Status {status} \n".format(
            number=self.num,
            name=self.name,
            status=status_all))

        for microtest in self.data:
            if microtest["status"]:
                microtest_status = colored(microtest["status"], "green")
            else:
                microtest_status = colored(microtest["status"], "red")
            print("\t\t {index}. {name}. Status {status}. Time {time:.3f}s \n".format(
                index=microtest["index"],
                name=microtest["name"],
                status=microtest_status,
                time=microtest["time_work"]))
            if microtest["message"]:
                print("\t\t\t {message} \n".format(message=microtest["message"]))

    def __save_result(self):
        my_file = open("test_write_file.html", "a", encoding="utf-16")
        if self.status:
            status_all_html = "<font style='color:#00ff00'> {s} </font>".format(s=self.status)
        else:
            status_all_html = "<font style='color:#ff0000'> {s} </font>".format(s=self.status)
        test_string = ("test {number}. {name}. Status {status} </br>".format(
                        number=self.num,
                        name=self.name,
                        status=status_all_html))
        my_file.write(test_string)

        for microtest in self.data:
            if microtest["status"]:
                microtest_string_html = "<font style='color:#00ff00'> {s} </font>".format(s=microtest["status"])
            else:
                microtest_string_html = "<font style='color:#ff0000'> {s} </font>".format(s=microtest["status"])
            microtest_string = ("<span style='padding: 0px 40px;'>{index}. {name}. Status {status}. Time {time:.3f}s</span></br>".format(
                                index=microtest["index"],
                                name=microtest["name"],
                                status=microtest_string_html,
                                time=microtest["time_work"]))
            my_file.write(microtest_string)
            if microtest["message"]:

                mes = microtest["message"].replace("\n", "</br>")
                mes = mes.replace("\t\t\t", "<span style='padding: 0px 40px;'></span>")
                mes_string = ("<span style='padding: 0px 80px;'>{message} </span>".format(message=mes))
                my_file.write(mes_string)
            my_file.write("</br>")

        my_file.close()

    def start(self):
        start = 65  # если количсество микротестов больше 26 - придумать как добавлять АААААА
        for index, input in enumerate(self.microtest_data):
            input["struct_microtest"]["index"] = chr(start + index)
            microtest = baseMicrotest(
                headers=input["headers"],
                struct_microtest=input["struct_microtest"],
                type=input["type"],
                url=input["url"],
                json_entry=input["json_entry"],
                params=input["params"],
                response_code=input["response_code"],
                check_for_output=input["dict_check_for_output"],
                base_url=self.base_url
            )
            microtest.start()
            if microtest.microtest["status"] is False:
                self.status = False
            self.data.append(microtest.microtest)

        self.__print_format()
        self.__save_result()