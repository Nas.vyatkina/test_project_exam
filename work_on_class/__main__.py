from work_on_class.functions import check_input_data
from work_on_class.input_data import microtest_data

from work_on_class.exam_unit_test_class import baseTest

base_url = "http://178.159.110.21:73/exzam/api/mobile/v1"

if __name__ == "__main__":
    check_input_data(data=microtest_data)

    for num_test, test in enumerate(microtest_data):
        test = baseTest(
            name=test["name"],
            num=num_test + 1,
            microtest_data=test["data"],
            base_url=base_url)
        test.start()