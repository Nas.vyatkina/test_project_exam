#  для проверки того что данные которые мы ожидаем коректны по структуре
def check_input_data(data):
    assert type(data) == list
    for test in data:
        assert type(test) == dict
        assert type(test["name"]) == str
        for microtest in test["data"]:
            assert type(microtest) == dict

            assert type(microtest["headers"]) == dict
            assert type(microtest["json_entry"]) == dict
            assert type(microtest["response_code"]) == dict
            assert type(microtest["dict_check_for_output"]) == dict
            assert type(microtest["type"]) == str
            assert microtest["type"] in ["POST", "GET"]