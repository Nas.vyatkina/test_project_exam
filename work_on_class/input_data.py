from work_on_class.exam_unit_test_class import Token

url_user_entry = "/user/entry/social"
url_user_info = "/user/info"
url_user_update = "/user/update"
url_user_logout = "/user/logout"

url_faculty = "/faculty/university"
url_faculty_course_subject = "/faculty/course/subject"

url_subject_seminar = "/subject/seminars"
url_subject_seminar_questions = "/subject/seminar/questions"
url_subject_seminar_question = "/subject/seminar/question"

url_subject_exams = "/subject/exams"
url_subject_exam_question = "/subject/exam/question"

url_subject_course_works = "/subject/course_works"
url_subject_course_work_order = "/subject/course_work/order"

url_favorites = "/favorites"
url_favorites_with_answer = "/favorites/with_answer"
url_favorite_add = "/favorite/add"
url_favorite_remove = "/favorite/remove"
url_favorite_questions = "/favorite/questions"
url_favorite_drop = "/favorite/drop"


# обновление токена из заголовка ответа
def token_update(headers):
    global token
    try:
        token.token = headers["Token"]
    except KeyError:
        print("error except token key error")


def replacement(data, keys):
    check_type = {
        "list": list,
        "str": str,
        "dict": dict,
        "int": int,
        "bool": bool
    }

    for key in keys:
        type_now = data[key]['type']
        data[key]['type'] = check_type[type_now]
        if data[key]['next'] is not None:
            if type(data[key]["next"]) == list:
                keys_next = range(len(data[key]["next"]))
            else:
                keys_next = data[key]["next"].keys()
            replacement(data[key]["next"], keys_next)
    return data


token = Token()

microtest_data =[
    {
        "data": [
            {
                "headers": {"Content_Type": "application/json"},
                "struct_microtest": {"name": "Корректные данные"},
                "type": "POST",
                "url": url_user_entry,
                "json_entry": {
                    "social": "FB",
                    "token_social": "EAAMYGrdEr3UBAOL0f5lUkAdAzyuc2oM6CNKOH9SmKOQPMufNV1t3zTtbHJZAf38Oc4XVXvbTjZCA4PMCuftybIQ7AVUkwmu6bBmbL0OHmBphcgaG0IAPgWjUjUd97ZBntQyjrpZC1yWExYIE2budqW2XPFfeFtOclaZBOiZB4PJYy2N902b2MIussXcLh9nTM0D1Fv9JHyF0ZAHpFFogXKuch8RgWFPalVwjJiuKHUbZAXQOgCIwTen6"},
                "params": {},
                "response_code": {
                    1: {
                        "action": None,
                        "func": token_update},
                    "else": {
                        "action": False,
                        "func": None}},
                "dict_check_for_output": {
                    "data": {
                        "next": {
                            "user": {
                                "next": {
                                    "title_course": {"next": None, "type": "str"},
                                    "id_user": {"next": None, "type": "int"},
                                    "is_update_info": {"next": None, "type": "bool"},
                                    "title_faculty": {"next": None, "type": "str"},
                                    "id_course_faculty": {"next": None, "type": "str"},
                                    "full_name": {"next": None, "type": "str"},
                                    "id_course": {"next": None, "type": "int"},
                                    "title_social": {"next": None, "type": "str"},
                                    "id_faculty": {"next": None, "type": "int"},
                                    "email": {"next": None, "type": "str"},
                                    "id_in_social": {"next": None, "type": "str"},
                                    "is_subscription": {"next": None, "type": "bool"}},
                                "type": "dict"}},
                        "type": "dict"},
                    "message": {
                        "next": {
                            "body": {"next": None, "type": "str"},
                            "code": {"next": None, "type": "int"},
                            "title": {"next": None, "type": "str"}},
                        "type": "dict"},
                    "code": {"next": None, "type": "int"}}
            },
            {
                "headers": { "Content_Type": "application/json"},
                "struct_microtest":{
                    "name": "Невалидный токен социальной сети/невалидная соц. сеть"},
                "type": "POST",
                "url": url_user_entry,
                "json_entry": {
                    "social": "FB",
                    "token_social": "test"},
                "params": {},
                "response_code": {
                    -1: {
                        "action": None,
                        "func": None},
                    1: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": { "Content_Type": "application/json"},
                "struct_microtest":{
                    "name": "Отсутствие необходимого аргумета/пустое значение аргумента"},
                "type": "POST",
                "url": url_user_entry,
                "json_entry": {
                    "social": "FB",
                    "token_social": ""},
                "params": {},
                "response_code": {
                    -33: {
                        "action": None,
                        "func": None},
                    -35: {
                        "action": None,
                        "func": None},
                    1: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            }],
        "name": "Проверка входа"},
    {
        "data": [
             {
                 "headers": {
                     "Content_Type": "application/json",
                     "Token": token.get},
                 "struct_microtest": {
                     "name": "Получение корректных данных по токену"},
                 "type": "GET",
                 "url": url_user_info,
                 "json_entry": {},
                 "params": {},
                 "response_code": {
                     1: {
                         "action": None,
                         "func": None},
                     "else": {
                         "action": False,
                         "func": None}},
                 "dict_check_for_output": {
                     "data": {
                         "type": "dict",
                         "next": {
                             "user": {
                                 "type": "dict",
                                 "next": {
                                     "is_update_info": {"type": "bool", "next": None},
                                     "title_faculty": {"type": "str", "next": None},
                                     "id_course_faculty": {"type": "str", "next": None},
                                     "is_subscription": {"type": "bool", "next": None},
                                     "id_in_social": {"type": "str", "next": None},
                                     "id_faculty": {"type": "int", "next": None},
                                     "id_course": {"type": "int", "next": None},
                                     "full_name": {"type": "str", "next": None},
                                     "email": {"type": "str", "next": None},
                                     "title_social": {"type": "str", "next": None},
                                     "title_course": {"type": "str", "next": None},
                                     "id_user": {"type": "int", "next": None}}}}},
                     "message": {
                         "type": "dict",
                         "next": {
                             "code": {"type": "int", "next": None},
                             "body": {"type": "str", "next": None},
                             "title": {"type": "str", "next": None}}},
                     "code": {"type": "int", "next": None}}
             },
             {
                 "headers": {
                     "Content_Type": "application/json",
                     "Token": "fszf"},
                 "struct_microtest": {"name": "Невалидный токен"},
                 "type": "GET",
                 "url": url_user_info,
                 "json_entry": {},
                 "params": {},
                 "response_code": {
                     -32: {
                         "action": None,
                         "func": None},
                     1: {
                         "action": False,
                         "func": None},
                     "else": {
                         "action": True,
                         "func": None}},
                 "dict_check_for_output": {}
             },
             {
                 "headers": {
                     "Content_Type": "application/json"},
                 "struct_microtest": {"name": "Недостаточно аргументов"},
                 "type": "GET",
                 "url": url_user_info,
                 "json_entry": {},
                 "params": {},
                 "response_code": {
                     -34: {
                         "action": None,
                         "func": None},
                     1: {
                         "action": False,
                         "func": None},
                     "else": {
                         "action": True,
                         "func": None}},
                 "dict_check_for_output": {}
             }],
        "name": "Проверка получения информации о пользователе"},
    {
        "data": [
            {
                "headers": {
                    "Content_Type": "application/json",
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Успешное обновление (входящие параметры корректны)"},
                "type": "POST",
                "url": url_user_update,
                "json_entry": {
                    "id_course_faculty": 30},
                    # "full_name": "string",
                    # "email": ""},
                "params": {},
                "response_code": {
                    1: {
                        "action": None,
                        "func": token_update},
                    "else": {
                        "action": False,
                        "func": None}},
                "dict_check_for_output": {
                    "message": {
                        "type": "dict",
                        "next": {
                            "title": {"type": "str", "next": None},
                            "body": {"type": "str", "next": None},
                            "code": {"type": "int", "next": None}}},
                    "data": {
                        "type": "dict",
                        "next": {
                            "user": {
                                "type": "dict",
                                "next": {
                                    "id_faculty": {"type": "int", "next": None},
                                    "id_in_social": {"type": "str", "next": None},
                                    "id_course": {"type": "int", "next": None},
                                    "title_faculty": {"type": "str", "next": None},
                                    "title_course": {"type": "str", "next": None},
                                    "email": {"type": "str", "next": None},
                                    "id_course_faculty": {"type": "str", "next": None},
                                    "is_subscription": {"type": "bool", "next": None},
                                    "id_user": {"type": "int", "next": None},
                                    "full_name": {"type": "str", "next": None},
                                    "title_social": {"type": "str", "next": None},
                                    "is_update_info": {"type": "bool", "next": None}}}}},
                    "code": {"type": "int", "next": None}}
            },
            {
                "headers": {
                    "Content_Type": "application/json",
                    "Token": "fsdf"},
                "struct_microtest": {"name": "Невалидный токен"},
                "type": "POST",
                "url": url_user_update,
                "json_entry": {
                    "id_course_faculty": 30},
                    # "full_name": "string",
                    # "email": ""},
                "params": {},
                "response_code": {
                    -32: {
                        "action": None,
                        "func": None},
                    1: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {"Content_Type": "application/json"},
                "struct_microtest": {"name": "Недостаточно аргументов"},
                "type": "POST",
                "url": url_user_update,
                "json_entry": {
                    "id_course_faculty": 30},
                    # "full_name": "string",
                    # "email": ""},
                "params": {},
                "response_code": {
                    -33: {
                        "action": None,
                        "func": None},
                    1: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            }],
        "name": "Проверка изменения данных о пользователе"},
    {
        "data": [
            {
                "headers": {"Content_Type": "application/json"},
                "struct_microtest": {
                    "name": "Успешное получение списка факультетов"},
                "type": "GET",
                "url": url_faculty,
                "json_entry": {},
                "params": {"id_university": 1},
                "response_code": {
                    5: {
                    "action": None,
                    "func": None},
                    "else": {
                        "action": False,
                        "func": None}},
                "dict_check_for_output": {
                    "data": {
                        "next": {
                            "faculties": {
                                "next": [
                                    {"next": {
                                        "courses": {
                                            "next": [
                                                {"next": {
                                                    "id_course": {"next": None, "type": "int"},
                                                    "id_course_faculty": {"next": None, "type": "int"},
                                                    "title_course": {"next": None, "type": "str"}},
                                                "type": "dict"}],
                                            "type": "list"},
                                        "id_facuty": {"next": None, "type": "int"},
                                        "title_faculty": {"next": None, "type": "str"},
                                        "path_to_image": {"next": None, "type": "str"}},
                                    "type": "dict"}],
                                "type": "list"}},
                        "type": "dict"},
                    "code": {"next": None, "type": "int"},
                    "message": {
                        "next": {
                            "title": {"next": None, "type": "str"},
                            "code": {"next": None, "type": "int"},
                            "body": {"next": None, "type": "str"}},
                        "type": "dict"}}
            },
            {
                "headers": {"Content_Type": "application/json"},
                "struct_microtest": {
                    "name": "айди университета - строка (неизвестная ошибка)"},
                "type": "GET",
                "url": url_faculty,
                "json_entry": {},
                "params": {"id_university": "i"},
                "response_code": {
                    -31: {
                        "action": None,
                        "func": None},
                    5: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            }],
        "name": "Получение списка факультетов"},
    {
        "data": [
            {
                "headers": {"Token": token.get},
                "struct_microtest": {
                    "name": "Успешное получение списка предметов пользователя"},
                "type": "GET",
                "url": url_faculty_course_subject,
                "json_entry": {},
                "params": {"type_work": "course_work"},
                "response_code": {
                    7: {
                        "action": None,
                        "func": None},
                    "else": {
                        "action": False,
                        "func": None}},
                "dict_check_for_output": {
                    "message": {
                        "next": {
                            "title": {"next": None, "type": "str"},
                            "code": {"next": None, "type": "int"},
                            "body": {"next": None, "type": "str"}},
                        "type": "dict"},
                    "data": {
                        "next": {
                            "subjects": {
                                "next": [{
                                    "next": {
                                        "is_enabled": {"next": None, "type": "bool"},
                                        "title_subject": {"next": None, "type": "str"},
                                        "id_course_subject": {"next": None, "type": "int"}},
                                    "type": "dict"}],
                                "type": "list"},
                            "type_work": {"next": None, "type": "str"}},
                        "type": "dict"},
                    "code": {"next": None, "type": "int"}}
            },
            {
                "headers": {"Token": "fwef"},
                "struct_microtest": {"name": "Невалидный токен"},
                "type": "GET",
                "url": url_faculty_course_subject,
                "json_entry": {},
                "params": {"type_work": "course_work"},
                "response_code": {
                    -32: {
                        "action": None,
                        "func": None},
                    7: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {"Token": token.get},
                "struct_microtest": {
                    "name": "Недостаточно аргументов для выполнения запроса"},
                "type": "GET",
                "url": url_faculty_course_subject,
                "json_entry": {},
                "params": {},
                "response_code": {
                    -33: {
                        "action": None,
                        "func": None},
                    7: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {"Token": token.get},
                "struct_microtest": {"name": "Неверный тип работы"},
                "type": "GET",
                "url": url_faculty_course_subject,
                "json_entry": {},
                "params": {"type_work": "sssfsdfs"},
                "response_code": {
                    -10: {
                        "action": None,
                        "func": None},
                    7: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            }],
        "name": "Получение списка предметов пользователя"},
    {
        "data": [
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Успешное получение списка предметов пользователя"},
                "type": "GET",
                "url": url_subject_seminar,
                "json_entry": {},
                "params": {"id_subject": 38},
                "response_code": {
                    8: {
                        "action": None,
                        "func": None},
                    "else": {
                        "action": False,
                        "func": None}},
                "dict_check_for_output": {
                    "code": {"type": "int", "next": None},
                    "message": {
                        "type": "dict",
                        "next": {
                            "title": {"type": "str", "next": None},
                            "code": {"type": "int", "next": None},
                            "body": {"type": "str", "next": None}}},
                    "data": {
                        "type": "dict",
                        "next": {
                            "seminars": {
                                "type": "list",
                                "next": [{
                                    "type": "dict",
                                    "next": {
                                        "theme_seminar": {"type": "str", "next": None},
                                        "quantity_ask": {"type": "int", "next": None},
                                        "id_seminar": {"type": "int", "next": None}}}]}}}}
            },
            {
                "headers": {
                    "Token": "fwef"},
                "struct_microtest": {
                    "name": "Невалидный токен"},
                "type": "GET",
                "url": url_subject_seminar,
                "json_entry": {},
                "params": {"id_subject": 38},
                "response_code": {
                    -32: {
                        "action": None,
                        "func": None},
                    8: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Предмет, для которого нет семинара"},
                "type": "GET",
                "url": url_subject_seminar,
                "json_entry": {},
                "params": {"id_subject": 54},
                "response_code": {
                    -12: {
                        "action": None,
                        "func": None},
                    8: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Недостаточно аргументов для выполнения запроса"},
                "type": "GET",
                "url": url_subject_seminar,
                "json_entry": {},
                "params": {},
                "response_code": {
                    -33: {
                        "action": None,
                        "func": None},
                    8: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            }],
        "name": "Получение семинаров для курса пользователя"},
    {
        "data": [
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Успешное получение вопросов"
                },
                "type": "GET",
                "url": url_subject_seminar_questions,
                "json_entry": {},
                "params": {"id_seminar": 53},
                "response_code": {
                    9: {
                        "action": None,
                        "func": None},
                    "else": {
                        "action": False,
                        "func": None}},
                "dict_check_for_output": {
                    "message": {
                        "next": {
                            "title": {"next": None, "type": "str"},
                            "code": {"next": None, "type": "int"},
                            "body": {"next": None, "type": "str"}},
                        "type": "dict"},
                    "code": {"next": None, "type": "int"},
                    "data": {
                        "next": {
                            "questions": {
                                "next": [{
                                    "next": {
                                        "title": {"next": None, "type": "str"},
                                        "id_object": {"next": None, "type": "int"},
                                        "hash": {"next": None, "type": "str"}},
                                    "type": "dict"}],
                                "type": "list"},
                            "tasks": {
                                "next": [{
                                    "next": {
                                        "title": {"next": None, "type": "str"},
                                        "id_object": {"next": None, "type": "int"},
                                        "hash": {"next": None, "type": "str"}},
                                    "type": "dict"}],
                                "type": "list"}},
                        "type": "dict"}}
            },
            {
                "headers": {
                    "Token": "fwef"},
                "struct_microtest": {
                    "name": "Невалидный токен"},
                "type": "GET",
                "url": url_subject_seminar_questions,
                "json_entry": {},
                "params": {"id_seminar": 53},
                "response_code": {
                    -32: {
                        "action": None,
                        "func": None},
                    9: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Нет вопросов для выбранного семинара"},
                "type": "GET",
                "url": url_subject_seminar_questions,
                "json_entry": {},
                "params": {"id_seminar": 32},
                "response_code": {
                    -13: {
                        "action": None,
                        "func": None},
                    9: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Недостаточно аргументов для выполнения запроса"},
                "type": "GET",
                "url": url_subject_seminar_questions,
                "json_entry": {},
                "params": {},
                "response_code": {
                    -33: {
                        "action": None,
                        "func": None},
                    9: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            }],
        "name": "Получение вопросов семинара"},
    {
        "data": [
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Успешное получение вопроса"
                },
                "type": "GET",
                "url": url_subject_seminar_question,
                "json_entry": {},
                "params": {"id_question": 95},
                "response_code": {
                    10: {
                        "action": None,
                        "func": None},
                    "else": {
                        "action": False,
                        "func": None}},
                "dict_check_for_output": {
                    "message": {
                        "next": {
                            "code": {"next": None, "type": "int"},
                            "body": {"next": None, "type": "str"},
                            "title": {"next": None, "type": "str"}},
                        "type": "dict"},
                    "code": {"next": None, "type": "int"},
                    "data": {
                        "next": {
                            "question": {
                                "next": {
                                    "id_object": {"next": None, "type": "int"},
                                    "answer": {"next": None, "type": "str"},
                                    "hash": {"next": None, "type": "str"},
                                    "title": {"next": None, "type": "str"},
                                    "type": {"next": None, "type": "str"}},
                                "type": "dict"}},
                        "type": "dict"}}
            },
            {
                "headers": {
                    "Token": "fwef"},
                "struct_microtest": {
                    "name": "Невалидный токен"},
                "type": "GET",
                "url": url_subject_seminar_question,
                "json_entry": {},
                "params": {"id_question": 95},
                "response_code": {
                    -32: {
                        "action": None,
                        "func": None},
                    10: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "По выбранному id вопрос не найден"},
                "type": "GET",
                "url": url_subject_seminar_question,
                "json_entry": {},
                "params": {"id_question": 2},
                "response_code": {
                    -14: {
                        "action": None,
                        "func": None},
                    10: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Недостаточно аргументов для выполнения запроса"},
                "type": "GET",
                "url": url_subject_seminar_question,
                "json_entry": {},
                "params": {},
                "response_code": {
                    -33: {
                        "action": None,
                        "func": None},
                    10: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            }],
        "name": "Получение вопроса семинара"},
    {
        "data": [
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Успешное получение вопросов"
                },
                "type": "GET",
                "url": url_subject_exams,
                "json_entry": {},
                "params": {"id_subject": 34},
                "response_code": {
                    11: {
                        "action": None,
                        "func": None},
                    "else": {
                        "action": False,
                        "func": None}},
                "dict_check_for_output": {
                    "message": {
                        "type": "dict",
                        "next": {
                            "title": {"type": "str", "next": None},
                            "body": {"type": "str", "next": None},
                            "code": {"type": "int", "next": None}}},
                    "code": {"type": "int", "next": None},
                    "data": {
                        "type": "dict",
                        "next": {
                            "exams": {
                                "type": "list",
                                "next": [{
                                    "type": "dict",
                                    "next": {
                                        "title": {"type": "str", "next": None},
                                        "id_exam": {"type": "int", "next": None},
                                        "hash": {"type": "str", "next": None}}}]}}}}
            },
            {
                "headers": {
                    "Token": "fwef"},
                "struct_microtest": {
                    "name": "Невалидный токен"},
                "type": "GET",
                "url": url_subject_exams,
                "json_entry": {},
                "params": {"id_subject": 95},
                "response_code": {
                    -32: {
                        "action": None,
                        "func": None},
                    11: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Нет вопросов для выбранного экзамена"},
                "type": "GET",
                "url": url_subject_exams,
                "json_entry": {},
                "params": {"id_subject": 2},
                "response_code": {
                    -415: {
                        "action": None,
                        "func": None},
                    11: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Недостаточно аргументов для выполнения запроса"},
                "type": "GET",
                "url": url_subject_exams,
                "json_entry": {},
                "params": {},
                "response_code": {
                    -33: {
                        "action": None,
                        "func": None},
                    11: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            }],
        "name": "Получение вопросов экзамена"},
    {
        "data": [
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Успешное получение вопроса"
                },
                "type": "GET",
                "url": url_subject_exam_question,
                "json_entry": {},
                "params": {"id_question_exam": 40},
                "response_code": {
                    12: {
                        "action": None,
                        "func": None},
                    "else": {
                        "action": False,
                        "func": None}},
                "dict_check_for_output": {
                    "data": {
                        "type": "dict",
                        "next": {
                            "exam_question": {
                                "type": "dict",
                                "next": {
                                    "hash": {"type": "str", "next": None},
                                    "answer": {"type": "str", "next": None},
                                    "id_object": {"type": "int", "next": None},
                                    "title": {"type": "str", "next": None}}}}},
                    "message": {
                        "type": "dict",
                        "next": {
                            "title": {"type": "str", "next": None},
                            "code": {"type": "int", "next": None},
                            "body": {"type": "str", "next": None}}},
                    "code": {"type": "int", "next": None}}
            },
            {
                "headers": {
                    "Token": "fwef"},
                "struct_microtest": {
                    "name": "Невалидный токен"},
                "type": "GET",
                "url": url_subject_exam_question,
                "json_entry": {},
                "params": {"id_question_exam": 40},
                "response_code": {
                    -32: {
                        "action": None,
                        "func": None},
                    12: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "По выбранному id вопрос не найден"},
                "type": "GET",
                "url": url_subject_exam_question,
                "json_entry": {},
                "params": {"id_question_exam": 2},
                "response_code": {
                    -16: {
                        "action": None,
                        "func": None},
                    12: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Недостаточно аргументов для выполнения запроса"},
                "type": "GET",
                "url": url_subject_exam_question,
                "json_entry": {},
                "params": {},
                "response_code": {
                    -35: {
                        "action": None,
                        "func": None},
                    12: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            }],
        "name": "Получение вопроса экзамена"},
    {
        "data": [
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Успешное получение курсовых"
                },
                "type": "GET",
                "url": url_subject_course_works,
                "json_entry": {},
                "params": {"id_subject": 34},
                "response_code": {
                    13: {
                        "action": None,
                        "func": None},
                    "else": {
                        "action": False,
                        "func": None}},
                "dict_check_for_output": {
                    "code": {"type": "int", "next": None},
                    "data": {
                        "type": "dict",
                        "next": {
                            "course_works": {
                                "type": "list",
                                "next": [{
                                    "type": "dict",
                                    "next": {
                                        "id_course_work": {"type": "int", "next": None},
                                        "title": {"type": "str", "next": None},
                                        "description": {"type": "str", "next": None},
                                        "price": {"type": "int", "next": None}}}]}}},
                    "message": {
                        "type": "dict",
                        "next": {
                            "body": {"type": "str", "next": None},
                            "code": {"type": "int", "next": None},
                            "title": {"type": "str", "next": None}}}}
            },
            {
                "headers": {
                    "Token": "fwef"},
                "struct_microtest": {
                    "name": "Невалидный токен"},
                "type": "GET",
                "url": url_subject_course_works,
                "json_entry": {},
                "params": {"id_subject": 34},
                "response_code": {
                    436: {
                        "action": None,
                        "func": None},
                    13: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Недостаточно аргументов для выполнения запроса"},
                "type": "GET",
                "url": url_subject_course_works,
                "json_entry": {},
                "params": {},
                "response_code": {
                    437: {
                        "action": None,
                        "func": None},
                    13: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            }],
        "name": "Получение списка курсовых"},
    {
        "data": [
            {
                "headers": {
                    "Content_Type": "application/json",
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Успешное оформление заказа"},
                "type": "POST",
                "url": url_subject_course_work_order,
                "json_entry": {
                    "id_theme": 58,
                    "email": "de@gmail.com",
                    "phone": "string",
                    "name": "deniska",
                    "text": "string"},
                "params": {},
                "response_code": {
                    14: {
                        "action": None,
                        "func": None},
                    "else": {
                        "action": False,
                        "func": None}},
                "dict_check_for_output": {
                    "code": {"type": "int", "next": None},
                    "message": {
                        "type": "dict",
                        "next": {
                            "title": {"type": "str", "next": None},
                            "code": {"type": "int", "next": None},
                            "body": {"type": "str", "next": None}}}}
            },
            {
                "headers": {
                    "Content_Type": "application/json",
                    "Token": "fsdf"},
                "struct_microtest": {"name": "Невалидный токен"},
                "type": "POST",
                "url": url_subject_course_work_order,
                "json_entry": {
                    "id_theme": 58,
                    "email": "de@gmail.com",
                    "phone": "string",
                    "name": "deniska",
                    "text": "string"},
                "params": {},
                "response_code": {
                    -32: {
                        "action": None,
                        "func": None},
                    14: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
             },
            {
                "headers": {
                    "Content_Type": "application/json",
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Недостаточно аргументов для выполнения запроса"},
                "type": "POST",
                "url": url_subject_course_work_order,
                "json_entry": {
                    "email": "de@gmail.com",
                    "phone": "string",
                    "name": "deniska",
                    "text": "string"},
                "params": {},
                "response_code": {
                    -33: {
                        "action": None,
                        "func": None},
                    14: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            }],
        "name": "Оформление заказа курсовой"},
    {
        "data": [
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Успешное получение избранных вопросов"
                },
                "type": "GET",
                "url": url_favorites,
                "json_entry": {},
                "params": {"is_seminar_question": True},
                "response_code": {
                    17: {
                        "action": None,
                        "func": None},
                    "else": {
                        "action": False,
                        "func": None}},
                "dict_check_for_output": {
                    "code": {"type": "int", "next": None},
                    "data": {
                        "type": "dict",
                        "next": {
                            "favorites": {
                                "type": "dict",
                                "next": {
                                    "favorite_seminar": {
                                        "type": "list",
                                        "next": [{
                                            "type": "dict",
                                            "next": {
                                                "hash": {"type": "str", "next": None},
                                                "theme_question": {"type": "str", "next": None},
                                                "id_question": {"type": "int", "next": None},
                                                "title_subject": {"type": "str", "next": None}}}]},
                                    "favorite_exam": {
                                        "type": "list",
                                        "next": [{
                                            "type": "dict",
                                            "next": {
                                                "hash": {"type": "str", "next": None},
                                                "theme_question": {"type": "str", "next": None},
                                                "id_question": {"type": "int", "next": None},
                                                "title_subject": {"type": "str", "next": None}}}]}}}}},
                    "message": {
                        "type": "dict",
                        "next": {
                            "code": {"type": "int", "next": None},
                            "title": {"type": "str", "next": None},
                            "body": {"type": "str", "next": None}}}}
            },
            {
                "headers": {
                    "Token": "fwef"},
                "struct_microtest": {
                    "name": "Невалидный токен"},
                "type": "GET",
                "url": url_favorites,
                "json_entry": {},
                "params": {"is_seminar_question": True},
                "response_code": {
                    -32: {
                        "action": None,
                        "func": None},
                    17: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {},
                "struct_microtest": {
                    "name": "Недостаточно аргументов для выполнения запроса"},
                "type": "GET",
                "url": url_favorites,
                "json_entry": {},
                "params": {"is_seminar_question": True},
                "response_code": {
                    -33: {
                        "action": None,
                        "func": None},
                    17: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            }],
        "name": "Получение списка избранного"},
    {
        "data": [
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Успешное получение избранных вопросов с ответом"
                },
                "type": "GET",
                "url": url_favorites_with_answer,
                "json_entry": {},
                "params": {"is_seminar_question": True},
                "response_code": {
                    17: {
                        "action": None,
                        "func": None},
                    "else": {
                        "action": False,
                        "func": None}},
                "dict_check_for_output": {
                    "message":{
                        "next": {
                            "title": {"next": None, "type": "str"}, 
                            "code": {"next": None, "type": "int"}, 
                            "body": {"next": None, "type": "str"}}, 
                        "type": "dict"}, 
                    "code": {"next": None, "type": "int"}, 
                    "data": {
                        "next": {
                            "favorites": {
                                "next": {
                                    "favorite_seminar": {
                                        "next": [{
                                            "next": {
                                                "id_question": {"next": None, "type": "int"}, 
                                                "answer": {"next": None, "type": "str"}, 
                                                "theme_question": {"next": None, "type": "str"}, 
                                                "hash": {"next": None, "type": "str"}, 
                                                "title_subject": {"next": None, "type": "str"}}, 
                                            "type": "dict"}], 
                                        "type": "list"}, 
                                    "favorite_exam": {
                                        "next": [{
                                            "next": {
                                                "id_question": {"next": None, "type": "int"}, 
                                                "answer": {"next": None, "type": "str"}, 
                                                "theme_question": {"next": None, "type": "str"}, 
                                                "hash": {"next": None, "type": "str"}, 
                                                "title_subject": {"next": None, "type": "str"}}, 
                                            "type": "dict"}], 
                                        "type": "list"}}, 
                                "type": "dict"}}, 
                        "type": "dict"}}
            },
            {
                "headers": {
                    "Token": "fwef"},
                "struct_microtest": {
                    "name": "Невалидный токен"},
                "type": "GET",
                "url": url_favorites_with_answer,
                "json_entry": {},
                "params": {"is_seminar_question": True},
                "response_code": {
                    -32: {
                        "action": None,
                        "func": None},
                    17: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {},
                "struct_microtest": {
                    "name": "Недостаточно аргументов для выполнения запроса"},
                "type": "GET",
                "url": url_favorites_with_answer,
                "json_entry": {},
                "params": {"is_seminar_question": True},
                "response_code": {
                    -33: {
                        "action": None,
                        "func": None},
                    17: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            }],
        "name": "Получение списка избранного с ответом"},
    {
        "data": [
            {
                "headers": {
                    "Content_Type": "application/json",
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Успешное добавление вопроса в избранное"},
                "type": "POST",
                "url": url_favorite_add,
                "json_entry": {
                    "is_seminar_question": True,
                    "id_question": 134,
                    "is_answer_needed": True},
                "params": {},
                "response_code": {
                    15: {
                        "action": None,
                        "func": None},
                    "else": {
                        "action": False,
                        "func": None}},
                "dict_check_for_output": {
                    "data": {
                        "type": "dict",
                        "next": {
                            "question": {
                                "type": "dict",
                                "next": {
                                    "hash": {"type": "str", "next": None},
                                    "theme_question": {"type": "str", "next": None},
                                    "title_subject": {"type": "str", "next": None},
                                    "id_question": {"type": "int", "next": None}}}}},
                    "message": {
                        "type": "dict",
                        "next": {
                            "title": {"type": "str", "next": None},
                            "code": {"type": "int", "next": None},
                            "body": {"type": "str", "next": None}}},
                    "code": {"type": "int", "next": None}}
            },
            {
                "headers": {
                    "Content_Type": "application/json",
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Добавление в избранное вопроса, уже существующего в списке"},
                "type": "POST",
                "url": url_favorite_add,
                "json_entry": {
                    "is_seminar_question": True,
                    "id_question": 134,
                    "is_answer_needed": True},
                "params": {},
                "response_code": {
                    -18: {
                        "action": None,
                        "func": None},
                    15: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {
                    "Content_Type": "application/json",
                    "Token": "fsdf"},
                "struct_microtest": {"name": "Невалидный токен"},
                "type": "POST",
                "url": url_favorite_add,
                "json_entry": {
                    "is_seminar_question": True,
                    "id_question": 134,
                    "is_answer_needed": True},
                "params": {},
                "response_code": {
                    -32: {
                        "action": None,
                        "func": None},
                    15: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {
                    "Content_Type": "application/json",
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Недостаточно аргументов для выполнения запроса"},
                "type": "POST",
                "url": url_favorite_add,
                "json_entry": {
                    "id_question": 134},
                "params": {},
                "response_code": {
                    -33: {
                        "action": None,
                        "func": None},
                    15: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            }],
        "name": "Добавление в избранное"},
    {
        "data": [
            {
                "headers": {
                    "Content_Type": "application/json",
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Успешное получение вопросов по списку id"},
                "type": "POST",
                "url": url_favorite_questions,
                "json_entry": {
                    "is_seminar_question": True,
                    "list_questions": [
                        134,
                        32]},
                "params": {},
                "response_code": {
                    18: {
                        "action": None,
                        "func": None},
                    "else": {
                        "action": False,
                        "func": None}},
                "dict_check_for_output": {
                    "code": {"next": None, "type": "int"},
                    "data": {
                        "next": {
                            "questions": {
                                "next": [{
                                    "next": {
                                        "title": {"next": None, "type": "str"},
                                        "answer": {"next": None, "type": "str"},
                                        "hash": {"next": None, "type": "str"},
                                        "id_object": {"next": None, "type": "int"}},
                                    "type": "dict"}],
                                "type": "list"}},
                        "type": "dict"},
                    "message": {
                        "next": {
                            "code": {"next": None, "type": "int"},
                            "title": {"next": None, "type": "str"},
                            "body": {"next": None, "type": "str"}},
                        "type": "dict"}}
            },
            {
                "headers": {
                    "Content_Type": "application/json",
                    "Token": "fsdf"},
                "struct_microtest": { "name": "Невалидный токен"},
                "type": "POST",
                "url": url_favorite_questions,
                "json_entry": {
                    "is_seminar_question": True,
                    "list_questions": [
                        134]},
                "params": {},
                "response_code": {
                    -32: {
                        "action": None,
                        "func": None},
                    18: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {
                    "Content_Type": "application/json",
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Недостаточно аргументов для выполнения запроса"},
                "type": "POST",
                "url": url_favorite_questions,
                "json_entry": {
                    "list_questions": [
                        134]},
                "params": {},
                "response_code": {
                    -33: {
                        "action": None,
                        "func": None},
                    18: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {
                    "Content_Type": "application/json",
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Список доступных вопросов пуст"},
                "type": "POST",
                "url": url_favorite_questions,
                "json_entry": {
                    "list_questions": [
                        534534]},
                "params": {},
                "response_code": {
                    -21: {
                        "action": None,
                        "func": None},
                    18: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            }],
        "name": "Получение вопросов по списку id (в документации нет описания ошибок)"},
    {
        "data": [
            {
                "headers": {
                    "Content_Type": "application/json",
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Успешное удаление указанного вопроса из избранного"},
                "type": "POST",
                "url": url_favorite_remove,
                "json_entry": {
                    "is_seminar_question": True,
                    "list_questions": [
                        134]},
                "params": {},
                "response_code": {
                    16: {
                        "action": None,
                        "func": None},
                    "else": {
                        "action": False,
                        "func": None}},
                "dict_check_for_output": {
                    "code": {"next": None, "type": "int"},
                    "message": {
                        "next": {
                            "code": {"next": None, "type": "int"},
                            "title": {"next": None, "type": "str"},
                            "body": {"next": None, "type": "str"}},
                        "type": "dict"}}
            },
            {
                "headers": {
                    "Content_Type": "application/json",
                    "Token": "fsdf"},
                "struct_microtest": {"name": "Невалидный токен"},
                "type": "POST",
                "url": url_favorite_remove,
                "json_entry": {
                    "is_seminar_question": True,
                    "list_questions": [
                        134]},
                "params": {},
                "response_code": {
                    -32: {
                        "action": None,
                        "func": None},
                    16: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {
                    "Content_Type": "application/json",
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Недостаточно аргументов для выполнения запроса"},
                "type": "POST",
                "url": url_favorite_remove,
                "json_entry": {
                    "list_questions": [
                        134]},
                "params": {},
                "response_code": {
                    -33: {
                        "action": None,
                        "func": None},
                    16: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            }],
        "name": "Удаление указанного вопроса из избранного"},
    {
        "data": [
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Успешное удаление избранного"},
                "type": "GET",
                "url": url_favorite_drop,
                "json_entry": {},
                "params": {},
                "response_code": {
                    16: {
                        "action": None,
                        "func": None},
                    "else": {
                        "action": False,
                        "func": None}},
                "dict_check_for_output": {
                    "code": {"next": None, "type": "int"},
                    "message": {
                        "next": {
                            "code": {"next": None, "type": "int"},
                            "title": {"next": None, "type": "str"},
                            "body": {"next": None, "type": "str"}},
                        "type": "dict"},
                    "data": {"next": {}, "type": "dict"}}
            },
            {
                "headers": {
                    "Token": "fwef"},
                "struct_microtest": {
                    "name": "Невалидный токен"},
                "type": "GET",
                "url": url_favorite_drop,
                "json_entry": {},
                "params": {},
                "response_code": {
                    -32: {
                        "action": None,
                        "func": None},
                    16: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {},
                "struct_microtest": {
                    "name": "Недостаточно аргументов для выполнения запроса"},
                "type": "GET",
                "url": url_favorite_drop,
                "json_entry": {},
                "params": {},
                "response_code": {
                    -33: {
                        "action": None,
                        "func": None},
                    16: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            }],
        "name": "Удаление всего избранного (в документации нет описания ошибок)"},
    {
        "data": [
            {
                "headers": {
                    "Token": token.get},
                "struct_microtest": {
                    "name": "Успешный выход"},
                "type": "GET",
                "url": url_user_logout,
                "json_entry": {},
                "params": {},
                "response_code": {
                    2: {
                        "action": None,
                        "func": None},
                    "else": {
                        "action": False,
                        "func": None}},
                "dict_check_for_output": {
                    "message": {
                        "next": {
                            "body": {"next": None, "type": "str"},
                            "code": {"next": None, "type": "int"},
                            "title": {"next": None, "type": "str"}},
                        "type": "dict"},
                    "code": {"next": None, "type": "int"}}
            },
            {
                "headers": {
                    "Token": "fwef"},
                "struct_microtest": {
                    "name": "Невалидный токен (нет в документации)"},
                "type": "GET",
                "url": url_user_logout,
                "json_entry": {},
                "params": {},
                "response_code": {
                    -32: {
                        "action": None,
                        "func": None},
                    2: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            },
            {
                "headers": {},
                "struct_microtest": {
                    "name": "Недостаточно аргументов для выполнения запроса"},
                "type": "GET",
                "url": url_user_logout,
                "json_entry": {},
                "params": {},
                "response_code": {
                    -33: {
                        "action": None,
                        "func": None},
                    2: {
                        "action": False,
                        "func": None},
                    "else": {
                        "action": True,
                        "func": None}},
                "dict_check_for_output": {}
            }],
        "name": "Проверка выхода"}
]
for row in range(len(microtest_data)):
    for i in range(len(microtest_data[row]["data"])):
        data = microtest_data[row]["data"][i]["dict_check_for_output"]
        if data:
            replacement(data=data, keys=data.keys())